﻿using ClipperLib;
using System;
using Legend;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;
using X_UniTMX;
using System.Diagnostics;


namespace LegendOfLucaAdventureMod
{
    public class AdventureModEnemyTracker
    {

        // when enemies are damaged this is called
        public bool OnEnemyDamaged(Damageable damageable_cb, MonoBehaviour source_cb, LegendOfLucaAdventureMode mod_cb)
        {
                        
            if(damageable_cb == null)
            {
                LegendOfLucaAdventureMode.Instance.ModLogger("ERROR", "Damagable enemy was passed in as null in enemy tracker?");
                return false;
            }

            if(source_cb == null)
            {
                LegendOfLucaAdventureMode.Instance.ModLogger("ERROR", "Source callback entry was null in enemy tracker!?");
                return false;
            }

            if(mod_cb == null)
            {
                LegendOfLucaAdventureMode.Instance.ModLogger("Error", "Mod callback handle was null in enemy tracker?!"); ;
                return false;
            }
            if(Player.Instance == null)
            {
                LegendOfLucaAdventureMode.Instance.ModLogger("Error", "Player instance was null in enemy tracker"); ;
                return false;
            }

            // set our player instance
            Player player = Player.Instance;


            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Basic Enemy Evade Chance  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // create basic enemy evade chance
            System.Random enemy_evade_rng = new System.Random();

            // set enemy base evade chance
            float enemy_base_evade_chance = 30f;

            // reduce evade chance based on current dexterity 
            if (mod_cb.current_save_game.Dexterity >= 100) 
                enemy_base_evade_chance -= 5;
            if (mod_cb.current_save_game.Dexterity >= 200)
                enemy_base_evade_chance -= 5;
            if (mod_cb.current_save_game.Dexterity >= 300)
                enemy_base_evade_chance -= 5;
            if (mod_cb.current_save_game.Dexterity >= 400)
                enemy_base_evade_chance -= 2;
            if (mod_cb.current_save_game.Dexterity >= 500)
                enemy_base_evade_chance -= 2;
            if (mod_cb.current_save_game.Dexterity >= 600)
                enemy_base_evade_chance -= 1;
            if (mod_cb.current_save_game.Dexterity >= 700)
                enemy_base_evade_chance -= 1;
            if (mod_cb.current_save_game.Dexterity >= 800)
                enemy_base_evade_chance -= 1;
            if (mod_cb.current_save_game.Dexterity >= 900)
                enemy_base_evade_chance -= 1;
            if (mod_cb.current_save_game.Dexterity >= 1000)
                enemy_base_evade_chance -= 1;

            // if the enemy has evaded us, exit immediately
            if (enemy_base_evade_chance >= enemy_evade_rng.Next(1, 100))
            {
                return false;
            }
                



            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Check Status Effects %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // create rng for acting on status effects
            System.Random status_effect_rng = new System.Random();

            // give enemy a chance to evade if we're blind
            if (mod_cb.current_save_game.Blind)
            {
                // roll a new chance
                float blind_roll = status_effect_rng.Next(1, 100);
                if (mod_cb.current_save_game.BlindResist < blind_roll)
                {
                    HUD.ShowMessage("Blindness prevents you from striking your foe.", ".", 1.5f);
                    return false;
                }
                
            }


            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Melee Mode Life Steal Chance (After Enemy Evade )%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // create rng for life leech
            System.Random life_leech_rng = new System.Random();

            // set playe life leech chance
            float life_leech_chance = 10f;
            
            // if the player is using two melee weapons, add lifesteal chance
            if (mod_cb.PlayerIsUsingTwoMeleeWeapons())
            {

                if (life_leech_chance >= life_leech_rng.Next(1, 100))
                {
                    Player.Instance.Damageable.Heal(0.5f);
                }

            }


            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Check Weapon 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // check weapon one to see what type of weapon it is
            bool weapon_1_is_strength_based = false;
            bool weapon_1_is_dexterity_based = false;
            bool weapon_1_is_intelligence_based = false;
            bool weapon_1_is_unarmed = false;

            // check our first hand weapon for melee weapons
            if(player.Hands[0].CurrentWeapon != null)
            switch (player.Hands[0].CurrentWeapon.Type)
            {

                // check for melee strength based weapons
                case WeaponType.ClementiaAxe:
                case WeaponType.VeritasGreatsword:
                case WeaponType.VirtusSword:
                    weapon_1_is_strength_based = true;
                    break;

                // check for int based weapons
                case WeaponType.JustitiaStaff:
                case WeaponType.ComitasStaff:
                    weapon_1_is_intelligence_based = true;
                    break;

                // check for dex type weapons
                case WeaponType.DisiplinaBow:
                case WeaponType.FirmitasGlaive:
                    weapon_1_is_dexterity_based = true;
                    break;

                // we are unarmed with weapon 1
                default:
                    weapon_1_is_unarmed = true;
                    break;

            }

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Check Weapon 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // check weapon 2 to see what type of weapon it is
            bool weapon_2_is_strength_based = false;
            bool weapon_2_is_dexterity_based = false;
            bool weapon_2_is_intelligence_based = false;
            bool weapon_2_is_unarmed = false;

            // check our first hand weapon for melee weapons
            if(player.Hands[1].CurrentWeapon != null)
            switch (player.Hands[1].CurrentWeapon.Type)
            {

                // check for melee strength based weapons
                case WeaponType.ClementiaAxe:
                case WeaponType.VeritasGreatsword:
                case WeaponType.VirtusSword:
                    weapon_2_is_strength_based = true;
                    break;

                // check for int based weapons
                case WeaponType.JustitiaStaff:
                case WeaponType.ComitasStaff:
                    weapon_2_is_intelligence_based = true;
                    break;

                // check for dex type weapons
                case WeaponType.DisiplinaBow:
                case WeaponType.FirmitasGlaive:
                    weapon_2_is_dexterity_based = true;
                    break;

                // we are unarmed with weapon 1
                default:
                    weapon_2_is_unarmed = true;
                    break;

            }


            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Bonuses %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            
            if (weapon_1_is_strength_based && weapon_2_is_strength_based)
            {
                // if both weapons are strength based, increase strength
                mod_cb.current_save_game.Strength++;
            }
            else if (weapon_1_is_dexterity_based && weapon_2_is_dexterity_based)
            {
                // if both weapons are dex based, increase dex
                mod_cb.current_save_game.Dexterity++;
            }
            else if (weapon_1_is_intelligence_based && weapon_2_is_intelligence_based)
            {
                // if both weapons are intel based, increase intel
                mod_cb.current_save_game.Intelligence++;
            }
            else
            {
                // if weapons are mismatched, increase evasion
                mod_cb.current_save_game.Evasion++;
            }
            
            


            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Critical Strike Chance %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // critical strike chance for this attacked (based on Dex)
            float crit_chance = 0f;

            // this is used to hold our extra crit damage
            float extra_crit_damage = 0f;

            // this is a utility toggle to let us know further in our logic if we crit or not
            bool crit_occured = false;
            
            
            // calculate chance if we have dex
            if (mod_cb.current_save_game.Dexterity > 0)
            {

                // calculate the crit chance
                crit_chance = (mod_cb.current_save_game.Dexterity * 0.1f);
                
                // cap crit chance at 50%
                if (crit_chance > 50)
                    crit_chance = 50;

                //create new crit chance random selector
                System.Random crit_rand = new System.Random();
                
                // select a random value between 1 and 100
                int crit_rng = crit_rand.Next(1, 100);

                // if we got a crit, select extra damage
                if(crit_rng <= (crit_chance + mod_cb.current_save_game.LuckyCritOvercap))
                    crit_occured = true;
                    
                
            }

          
            
            // do extra damage if we have a rage bonus
            if (mod_cb.current_save_game.RageBonus > 0)
            {
                System.Random rage_message_rng = new System.Random();

                // create random chance
                int rage_damage_chance = rage_message_rng.Next(1, 15);

                // create random roll
                int rage_damage_roll = rage_message_rng.Next(1, 100);

                // if the damage roll is hit, apply rage
                if(rage_damage_roll <= rage_damage_chance)
                { 

                    switch (rage_message_rng.Next(1, 6))
                    { 
                        case 1:
                            HUD.ShowMessage("eXtrA-RagE!!", "Rudy Eugene becomes your spirit animal.", 3f);
                            break;
                        case 2:
                            HUD.ShowMessage("XXxeXtrA-RagExXX", "Not even doom music.", 3f);
                            break;
                        case 3:
                            HUD.ShowMessage("RAGE", "If wrath is a deadly sin, you become it's Las Vegas.", 2f);
                            break;
                        case 4:
                            HUD.ShowMessage("RRrrrrrraGE", "She said she was on birth control.", 2f);
                            break;
                        case 5:
                            HUD.ShowMessage("RaGeeEEe", "Nothing like a fireside sit by a raging inferno of hate.", 2f);
                            break;
                        case 6:
                            HUD.ShowMessage("RAAAaGe", "Blood for the blood god.", 2f);
                            break;
                        default:
                            HUD.ShowMessage("RaGgggGGGe", "That rage-bonus feeling you get when you're an ex-Trump supporter who could've better spent their time supporting a better candidate.", 2f);
                            break;
                    }

                    // peform rage damage
                    damageable_cb.Damage(0.25f, Vector3.zero, source_cb);

                }

            }

            // if we struck a crit, let the world know
            if (crit_occured)
            {
                
                // attempt to deal damage (50% extra damage)
                damageable_cb.Damage(0.5f, Vector3.zero, source_cb);
                                
                // create crit message
                // string crit_msg = String.Format("Crit Chance: {0}, Crit Multi: {1}, Total Extra Damage: {2}", crit_chance, 1 + crit_chance, extra_crit_damage);

                // show crit message
                HUD.ShowMessage("Critical Strike!", "!!!", 2f);

                // set crit sound
                string crit_cound = "swords_collide.wav";

                // attempt to play sound
                if (mod_cb.mod_audio.clips.ContainsKey(crit_cound))
                    Player.Instance.PlaySound(mod_cb.mod_audio.clips[crit_cound], 0.35f);
                else
                    mod_cb.ModLogger("ERROR", "Failed to play critical strike room sound.");

            }
            else
            {
               
                // Note: Strength used to add bonus damage, but it was pretty OP.  Changed it to give a 
                //       melee mode life regen bonus instead.  Code below commented out is old.

                // attempt to deal damage
                // damageable_cb.Damage(extra_strength_damage, position, source_cb);
                
                // create crit message
                // string non_crit_msg = String.Format("Damage: {0}, Extra Strength Bonus: {1}, Crit Chance: {2}", Player.Instance.Damage, extra_strength_damage, crit_chance);

                // show normal message
                // HUD.ShowMessage("Normal Attack!!", non_crit_msg, 3f);

            }

            // return indicating success 
            return true;

        }

        // track enemy deaths
        public void OnEnemyDie(Enemy enemy, LegendOfLucaAdventureMode mod_cb)
        {
            if (enemy == null)
                return;
            if (mod_cb == null)
                return;

            // increment the enemy kill count
            mod_cb.current_save_game.EnemyKillCount++;

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Level Up Mechanics %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            if(mod_cb.current_save_game.KillCountdownRemainingTillNextLevelUp <= 0)
            {

                // set kill count to level up counter (may modify this to be more liner/progressive in the future)
                mod_cb.current_save_game.KillCountdownRemainingTillNextLevelUp = 35;

                // create basic enemy evade chance
                System.Random level_up_rng = new System.Random();

                // add 50 to all stats
                mod_cb.current_save_game.Dexterity    += 25;
                mod_cb.current_save_game.Strength     += 25;
                mod_cb.current_save_game.Intelligence += 25;
                mod_cb.current_save_game.Evasion      += 25;

                // display current level stats
                String level_up_info = "\n\nLevel Current Stats: ";
                level_up_info += "\n Str: " + mod_cb.current_save_game.Strength + "";
                level_up_info += "\n Int: " + mod_cb.current_save_game.Intelligence + "";
                level_up_info += "\n Eva: " + mod_cb.current_save_game.Evasion + "";
                level_up_info += "\n Dex: " + mod_cb.current_save_game.Dexterity + "";

                // add random message for humor value (i'm not funny)
                switch (level_up_rng.Next(1, 10))
                {
                    case 1:
                        HUD.ShowMessage("Through epic time-wasting and non-real-world-productivity, you've managed to level up."+ level_up_info, "+25 all stats!", 6f);
                        break;
                    case 2:
                        HUD.ShowMessage("Level up!  Now you can finally beat this mod (ha, just kidding; mods hard)"+ level_up_info, "+25 all stats!", 6f);
                        break;
                    case 3:
                        HUD.ShowMessage("You've managed to level up but no one came to the party because they're afraid of your penchant for violence." + level_up_info, "+25 all stats!", 6f);
                        break;
                    case 4:
                        HUD.ShowMessage("As the damned soul rises, so does the fire.  Level up!" + level_up_info, "+25 all stats!", 6f);
                        break;
                    case 5:
                        HUD.ShowMessage("Level up!  You're lucky I don't let the mobs level up with you." + level_up_info, "+25 all stats!", 6f);
                        break;
                    case 6:
                        HUD.ShowMessage("Level up!  Link never leveled up.  That makes you better than him." + level_up_info, "+25 all stats!", 6f);
                        break;
                    case 7:
                        HUD.ShowMessage("Level up!  Your options for lateral movement within the company increase." + level_up_info, "+25 all stats!", 6f);
                        break;
                    case 8:
                        HUD.ShowMessage("Level up!  What did those mobs ever do to you, you psychopath." + level_up_info, "+25 all stats!", 6f);
                        break;
                    case 9:
                        HUD.ShowMessage("Level up!  Sadism pays off!" + level_up_info, "+25 all stats!", 6f);
                        break;
                    case 10:
                        HUD.ShowMessage("Level up!  Your deeds of valor will be remembered!" + level_up_info, "+25 all stats!", 6f);
                        break;
                    default:
                        break;

                }
                
            }
            else
            {
                mod_cb.current_save_game.KillCountdownRemainingTillNextLevelUp--;
            }

            

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Apply Savageness Bonus %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // savageness bonus
            if (mod_cb.current_save_game.EnemyKillCount % 500 == 0)
            {

                // display explorer bonus
                HUD.ShowMessage("BONUS! You've killed 500 enemies, you savage!", "+25 all stats!");

                // add 50 to all stats
                mod_cb.current_save_game.Dexterity    += 25;
                mod_cb.current_save_game.Strength     += 25;
                mod_cb.current_save_game.Intelligence += 25;
                mod_cb.current_save_game.Evasion      += 25;

            }
            else if (mod_cb.current_save_game.EnemyKillCount % 100 == 0)
            {

                // display explorer bonus
                HUD.ShowMessage("BONUS! You've killed 100 enemies and sent zero sympathy cards!", "+10 all stats!");

                // add 50 to all stats
                mod_cb.current_save_game.Dexterity    += 10;
                mod_cb.current_save_game.Strength     += 10;
                mod_cb.current_save_game.Intelligence += 10;
                mod_cb.current_save_game.Evasion      += 10;

            }

        }

        
    };

}