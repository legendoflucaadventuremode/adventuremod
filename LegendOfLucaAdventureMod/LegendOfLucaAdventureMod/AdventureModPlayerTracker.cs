﻿using ClipperLib;
using System;
using Legend;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;
using X_UniTMX;
using System.Diagnostics;

namespace LegendOfLucaAdventureMod
{

    // player tracker (tracks player being hit)
    public class AdventureModPlayerTracker
    {

        // attempt to process a player hit
        public OnPlayerHitResult OnPlayerHit(MonoBehaviour source_cb, LegendOfLucaAdventureMode mod_cb)
        {

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Calculate Dodge (Before Status Effects ) %%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            //create new evasion chance random selector
            System.Random dodge_rand = new System.Random();

            // select a random value between 1 and 100
            int dodge_rng = dodge_rand.Next(1, 100);

            // force dodge chance to 60 if it's over 60
            float dodge_chance = mod_cb.current_save_game.Evasion * 0.01f;
            if (dodge_chance > 50)
                dodge_chance = 50;

            // if we hit our chance margin, deal no damage
            if (dodge_chance > dodge_rng)
            {

                // set evasion chance
                string evaion_msg = String.Format("Evasion Chance: {0} percent", (mod_cb.current_save_game.Evasion * 0.01f));

                // show evade message
                HUD.ShowMessage("Evaded Enemy Attack!", evaion_msg, 1f);

                // show 
                return OnPlayerHitResult.IgnoreDamage;

            }

            
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Calculate Status Effects if No-Dodge Applied %%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // start the status tracker if it hasn't been started already
            if(!this.status_tracker_enabled)
                this.StartStatusTracker(mod_cb);

            //create new status_effect chance random selector
            System.Random status_effect_rand = new System.Random();

            // set chances here
            float bleeding_chance_percentage = 3;
            float poisoned_chance_percentage = 2;
            float burning_chance_percentage  = 2;
            float clumsy_chance_percentage   = 2;
            float blind_chance_percentage    = 2;

            // set durations here
            float bleed_length  = status_effect_rand.Next(4, 6);
            float poison_length = status_effect_rand.Next(4, 6);
            float burn_length   = status_effect_rand.Next(4, 6);
            float clumsy_length = status_effect_rand.Next(4, 6);
            float blind_length  = status_effect_rand.Next(4, 6);

            // start bleeding
            if (bleeding_chance_percentage >= status_effect_rand.Next(1, 100))
                this.StartBleeding(bleed_length);

            // start poison
            if (poisoned_chance_percentage >= status_effect_rand.Next(1, 100))
                this.StartPoison(poison_length);

            // start burning
            if (burning_chance_percentage >= status_effect_rand.Next(1, 100))
                this.StartBurning(burn_length);

            // start clumsy
            if (clumsy_chance_percentage >= status_effect_rand.Next(1, 100))
                this.StartClumsy(clumsy_length);

            // start blind 
            if (blind_chance_percentage >= status_effect_rand.Next(1, 100))
                this.StartBlind(blind_length);
            

            // deal damage to a player
            return OnPlayerHitResult.DealDamage;

        }


        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Enable/Disable Status Effects %%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // start burning if the player is on fire
        public void StartBurning(float burn_seconds)
        {

            // get the global singleton
            LegendOfLucaAdventureMode mod_cb = LegendOfLucaAdventureMode.Instance;

            // burns do not stack or extend each other, if not it could be OP with
            // other status effects being applied at the same time.
            if (mod_cb.current_save_game.Burning)
                return;

            // set burning flag to true
            mod_cb.current_save_game.Burning = true;

            // set burn time remaining
            mod_cb.current_save_game.BurnTimeRemaining = burn_seconds;

            // display status message
            HUD.ShowMessage("You have been ignited.  Burning causes you to lose health.", "");

            // set room color to red if we're on fire
            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.red;

            // set status sound
            string status_sound = "tortured_person_screaming_like_they_are_on_fire.wav";

            // attempt to play sound
            if (mod_cb.mod_audio.clips.ContainsKey(status_sound))
                Player.Instance.PlaySound(mod_cb.mod_audio.clips[status_sound], 0.35f);
            else
                mod_cb.ModLogger("ERROR", "Failed to play burning status sound.");

        }


        // start poison
        public void StartPoison(float poison_seconds)
        {

            // get the global mod singleton
            LegendOfLucaAdventureMode mod_cb = LegendOfLucaAdventureMode.Instance;

            // no stacking
            if (mod_cb.current_save_game.Poisoned)
                return;

            // set active flag to true
            mod_cb.current_save_game.Poisoned = true;

            // set time remaining
            mod_cb.current_save_game.PoisonTimeRemaining = poison_seconds;

            // display status message
            HUD.ShowMessage("You have been poisoned, you lose health health, and permanent stats.", "");

            // set room color to green if we've been poisoned
            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.green;

            // set status sound
            string status_sound = "gurgle_x.wav";

            // attempt to play sound
            if (mod_cb.mod_audio.clips.ContainsKey(status_sound))
                Player.Instance.PlaySound(mod_cb.mod_audio.clips[status_sound], 0.5f);
            else
                mod_cb.ModLogger("ERROR", "Failed to play poisoned status sound.");

        }

        // start bleeding
        public void StartBleeding(float bleed_seconds)
        {

            // get the global mod singleton
            LegendOfLucaAdventureMode mod_cb = LegendOfLucaAdventureMode.Instance;

            // no stacking
            if (mod_cb.current_save_game.Bleeding)
                return;

            // set active flag to true
            mod_cb.current_save_game.Bleeding = true;

            // set time remaining
            mod_cb.current_save_game.BleedingTimeRemaining = bleed_seconds;

            // display status message
            HUD.ShowMessage("The enemy has cut a vein and you are bleeding out.", "All global regens are disabled.");

            // set room color to grey if we've been bled
            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.grey;

            // set status sound
            string status_sound = "male_pain.wav";

            // attempt to play sound
            if (mod_cb.mod_audio.clips.ContainsKey(status_sound))
                Player.Instance.PlaySound(mod_cb.mod_audio.clips[status_sound], 0.5f);
            else
                mod_cb.ModLogger("ERROR", "Failed to play bleed status sound.");

        }

        // start clumsy
        public void StartClumsy(float clumsy_seconds)
        {

            // get the global mod singleton
            LegendOfLucaAdventureMode mod_cb = LegendOfLucaAdventureMode.Instance;

            // no stacking
            if (mod_cb.current_save_game.Clumsy)
                return;

            // set active flag to true
            mod_cb.current_save_game.Clumsy = true;

            // set time remaining
            mod_cb.current_save_game.ClumsyTimeRemaining = clumsy_seconds;

            // display status message
            HUD.ShowMessage("The enemy has struck you so hard that you feel clumsy.  You begin to drop items.", ".");

            // set room color to grey if we've been made clumsy
            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.yellow;

            // set status sound
            string status_sound = "ufo_landing_descending_woops.wav";

            // attempt to play sound
            if (mod_cb.mod_audio.clips.ContainsKey(status_sound))
                Player.Instance.PlaySound(mod_cb.mod_audio.clips[status_sound], 0.5f);
            else
                mod_cb.ModLogger("ERROR", "Failed to play clumsy status sound.");
            


        }

        // start blind
        public void StartBlind(float blind_seconds)
        {

            // get the global mod singleton
            LegendOfLucaAdventureMode mod_cb = LegendOfLucaAdventureMode.Instance;

            // no stacking
            if (mod_cb.current_save_game.Blind)
                return;

            // set active flag to true
            mod_cb.current_save_game.Blind = true;

            // set time remaining
            mod_cb.current_save_game.BlindTimeRemaining = blind_seconds;

            // display status message
            HUD.ShowMessage("You have been blinded by the enemy.", "Global light down, enemy evade up.");

            // set room color to grey if we've been made clumsy
            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.black;

            // set status sound
            string status_sound = "scream_and_die.wav";

            // attempt to play sound
            if (mod_cb.mod_audio.clips.ContainsKey(status_sound))
                Player.Instance.PlaySound(mod_cb.mod_audio.clips[status_sound], 0.5f);
            else
                mod_cb.ModLogger("ERROR", "Failed to play blind status sound.");

        }


        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Global Player Status Tracker %%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // switch to indicate whether or not the status tracker timer is running
        bool status_tracker_enabled = false;

        // status tracker 
        public System.Timers.Timer status_tracker_timer;

        // Call this to start the status tracker (enables status effects)
        public bool StartStatusTracker(LegendOfLucaAdventureMode mod_cb)
        {
            

            // ensure our status tracker hasn't already been started
            if (this.status_tracker_enabled == true)
                return true;
            else
                this.status_tracker_enabled = true;
            
            // log startup message
            mod_cb.ModLogger("NOTICE", "Player status tracker (status effects) enabled!");

            // return indicating success
            return true;

        }
        

    }

}
