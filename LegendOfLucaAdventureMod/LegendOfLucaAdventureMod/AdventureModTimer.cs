﻿using ClipperLib;
using System;
using Legend;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using UnityEngine;
using X_UniTMX;
using System.Diagnostics;

namespace LegendOfLucaAdventureMod
{

    // This class implements a timer in the main thread of the application.  DO NOT use
    // System.Timer to create a timer when modding.  System.Timer creates a new thread, and
    // unity is notoriously thread unfriendly.
    public class AdventureModTimer : MonoBehaviour
    {

       
        // timer started
        bool timer_started = false;

        // start global timer
        public void StartGlobalTimer()
        {
            
            // run basic check before starting timer
            if (LegendOfLucaAdventureMode.Instance == null)
                return;

            // log message
            LegendOfLucaAdventureMode.Instance.ModLogger("NOTICE", "Attempting to start global timer.");

            // start the co-routine
            StartCoroutine(Tick());

        }

        // ticks occur once a second
        IEnumerator Tick()
        {
            
            // enter global mod timer loop (executes once every second)
            if (this.timer_started != true)
            while (true)
            {


               // display log message on tick (optional)
               // LegendOfLucaAdventureMode.Instance.ModLogger("NOTICE", "Global timer is tick()ing..");

               // timer is always true once this loop starts
               this.timer_started = true;
                    
                // get the global singleton
                LegendOfLucaAdventureMode mod_cb = LegendOfLucaAdventureMode.Instance;



                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%% Status Effect Timer Ticks %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                if (mod_cb.player_tracker != null && mod_cb.current_save_game != null)
                { 

                    // create new random roller for status effects
                    System.Random status_effect_rng = new System.Random();


                    // IMPORTANT: Blind status effects are implemented in the enemy OnHit callback.

                    // handle blind status effects
                    if (mod_cb.current_save_game.Blind)
                    {

                        // decrease time remaining
                        mod_cb.current_save_game.BlindTimeRemaining -= 1f;

                        // reset blind parameters to default if time has elapsed
                        if (mod_cb.current_save_game.BlindTimeRemaining <= 0)
                        {

                            // reset switch/clock
                            mod_cb.current_save_game.Blind = false;
                            mod_cb.current_save_game.BlindTimeRemaining = 0;

                            // reset room color after expiration
                            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.white;

                        }

                    }


                    // burning effect (burning can kill a player, poison cannot)
                    if (mod_cb.current_save_game.Burning)
                    {

                        // decrease time remaining
                        mod_cb.current_save_game.BurnTimeRemaining -= 1f;

                        // reduce player health by 1 (this can kill)
                        Player.Instance.Damageable.Damage(1.0f, Vector3.zero, Player.Instance.Damageable);

                        // reset parameters to default if time has elapsed
                        if (mod_cb.current_save_game.BurnTimeRemaining <= 0)
                        {

                            // reset switch/clock
                            mod_cb.current_save_game.Burning = false;
                            mod_cb.current_save_game.BurnTimeRemaining = 0;

                            // reset room color after expiration
                            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.white;

                        }

                    }


                    // poisoned effect (player loses life up till two hearts; typically lasts longer than fire, also causes
                    // player status to be lowered)
                    if (mod_cb.current_save_game.Poisoned)
                    {

                        // decrease time remaining
                        mod_cb.current_save_game.PoisonTimeRemaining -= 1f;

                        // reduce a player to 2 health at a maximum 
                        if (Player.Instance.Damageable.Health >= 2)
                        {

                            // reduce player health by 1 (this can kill)
                            Player.Instance.Damageable.Damage(1.0f, Vector3.zero, Player.Instance.Damageable);

                        }

                        // degenerate strength permanently by 3
                        if (mod_cb.current_save_game.Strength >= 3)
                            mod_cb.current_save_game.Strength -= 3;

                        // degenerate dexterity permanently by 3
                        if (mod_cb.current_save_game.Dexterity >= 3)
                            mod_cb.current_save_game.Dexterity -= 3;

                        // degenerate intelligence permanently by 3
                        if (mod_cb.current_save_game.Intelligence >= 3)
                            mod_cb.current_save_game.Intelligence -= 3;

                        // degenerate evasion permanently by by 3
                        if (mod_cb.current_save_game.Evasion >= 3)
                            mod_cb.current_save_game.Evasion -= 3;

                        // reset parameters to default if time has elapsed
                        if (mod_cb.current_save_game.PoisonTimeRemaining <= 0)
                        {

                            // reset switch/clock
                            mod_cb.current_save_game.Poisoned = false;
                            mod_cb.current_save_game.PoisonTimeRemaining = 0;

                            // reset room color after expiration
                            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.white;

                        }

                    }

                    // IMPORTANT: Bleeding effect is implemented in MeleeMode timer.

                    // bleeding effect (player cannot regenerate melee mode life bonuses)
                    if (mod_cb.current_save_game.Bleeding)
                    {

                        // decrease time remaining
                        mod_cb.current_save_game.BleedingTimeRemaining -= 1f;

                        // reduce a player to 1 health at a maximum 
                        if (Player.Instance.Damageable.Health >= 2)
                        {

                            // reduce player health by 1 (this can kill)
                            Player.Instance.Damageable.Damage(0.5f, Vector3.zero, Player.Instance.Damageable);

                        }

                        // reset parameters to default if time has elapsed
                        if (mod_cb.current_save_game.BleedingTimeRemaining <= 0)
                        {

                            // reset switch/clock
                            mod_cb.current_save_game.Bleeding = false;
                            mod_cb.current_save_game.BleedingTimeRemaining = 0;

                            // reset room color after expiration
                            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.white;

                        }

                    }

                    // clumsy effect (randomly lose item types)
                    if (mod_cb.current_save_game.Clumsy)
                    {

                        // decrease time remaining
                        mod_cb.current_save_game.ClumsyTimeRemaining -= 1f;

                        // lose one (scaled) random item during status effect
                        switch (status_effect_rng.Next(1, 10))
                        {

                            // remove bomb
                            case 1:
                                if (Player.Instance.Bombs > 0)
                                {
                                    HUD.ShowMessage("You seem to have misplaced one of your bombs.");
                                    Player.Instance.Bombs--;
                                }
                                break;

                            // remove key
                            case 2:
                            case 3:
                            case 4:
                                if (Player.Instance.Keys > 0)
                                {
                                    HUD.ShowMessage("You seem to have misplaced one of your keys.");
                                    Player.Instance.Keys--;
                                }
                                break;

                            // remove coin
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 10:
                                if (Player.Instance.Coins > 0)
                                {
                                    HUD.ShowMessage("You seem to have misplaced one of your coins.");
                                    Player.Instance.Coins--;
                                }
                                break;

                            default:
                                break;

                        }

                        // reset parameters to default if time has elapsed
                        if (mod_cb.current_save_game.ClumsyTimeRemaining <= 0)
                        {

                            // reset switch/clock
                            mod_cb.current_save_game.Clumsy = false;
                            mod_cb.current_save_game.ClumsyTimeRemaining = 0;

                            // reset room color after expiration
                            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.white;

                        }

                    }

                }


                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%% Room Tracker Ticks %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                // create reference to our room tracker
                AdventureModRoomTracker room_tracker = mod_cb.room_tracker;

                if(room_tracker != null)
                {

                    // check room event states
                    bool damp_room_event_active = room_tracker.damp_room_event_active;
                    bool lucky_room_event_active = room_tracker.lucky_room_event_active;
                    bool poison_room_event_active = room_tracker.poison_room_event_active;
                    bool rage_room_event_active = room_tracker.rage_room_event_active;
                    bool blood_room_event_active = room_tracker.blood_room_event_active;
                    bool greed_room_event_active = room_tracker.greed_room_event_active;
                    bool armory_room_event_active = room_tracker.armory_room_event_active;
                        

                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    // %%% Handle Damp Room Event %%%%%%%%%%%%%%%%
                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    if (damp_room_event_active)
                    {

                        // reduce player bombs by 5 if they're over 10
                        if(Player.Instance.Bombs >= 10)
                        { 
                            Player.Instance.Bombs-=3;
                            HUD.ShowMessage("Three of your bombs have become soaked with water.");
                        }

                        /*
                        // Player.Instance.WeaponAmmos = 12f;
                        foreach (var weapon_ammo in Player.Instance.WeaponAmmos)
                        {
                            if (Player.Instance.WeaponAmmos[weapon_ammo.Key] > 0f)
                            {
                                Player.Instance.WeaponAmmos[weapon_ammo.Key] = 0f;
                            }
                        }

                        // set instance ammo to 12
                        if (Player.Instance.Ammo > 0f)
                        {
                            Player.Instance.Ammo = 0f;
                        }
                        */

                        // increment seconds elapsed after processing
                        room_tracker.damp_room_event_seconds_elapsed++;
                        

                    }

                    // disable damp bonus if time has elapsed
                    if (room_tracker.damp_room_event_seconds_elapsed >= 5)
                    {
                        room_tracker.damp_room_event_seconds_elapsed = 0;
                        room_tracker.damp_room_event_active = false;
                        HUD.ShowMessage("The room dries out.", ".", 1f);
                    }


                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    // %%% Handle Lucky Room Event %%%%%%%%%%%%%%%%
                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    if (lucky_room_event_active)
                    {

                        // set the crit overcap to 20%
                        mod_cb.current_save_game.LuckyCritOvercap = 20f;

                        // increment seconds elapsed after processing
                        room_tracker.lucky_room_event_seconds_elapsed++;

                    }

                    // disable lucky bonus if time has elapsed
                    if (room_tracker.lucky_room_event_seconds_elapsed >= 40)
                    {
                        room_tracker.lucky_room_event_seconds_elapsed = 0;
                        room_tracker.lucky_room_event_active = false;
                        mod_cb.current_save_game.LuckyCritOvercap = 0;
                        HUD.ShowMessage("Luck fades.", ".", 1f);
                    }


                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    // %%% Handle Poison Room Event %%%%%%%%%%%%%%%
                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    if (poison_room_event_active)
                    {

                        // increment seconds elapsed after processing
                        room_tracker.poison_room_event_seconds_elapsed++;

                        // degrade player health
                        Player.Instance.Damageable.Damage(0.5f, Vector3.zero, Player.Instance);

                    }

                    // disable poison 
                    if (room_tracker.poison_room_event_seconds_elapsed >= 7)
                    {
                        room_tracker.poison_room_event_seconds_elapsed = 0;
                        room_tracker.poison_room_event_active = false;
                    }


                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    // %%% Handle Rage Room Event %%%%%%%%%%%%%%%%%
                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    if (rage_room_event_active)
                    {

                        // set the rage bonus to 25%
                        mod_cb.current_save_game.RageBonus = 25f;

                        // increment seconds elapsed after processing
                        room_tracker.rage_room_event_seconds_elapsed++;


                    }

                    // disable lucky bonus if time has elapsed
                    if (room_tracker.rage_room_event_seconds_elapsed >= 40)
                    {
                        room_tracker.rage_room_event_seconds_elapsed = 0;
                        room_tracker.rage_room_event_active = false;
                        mod_cb.current_save_game.RageBonus = 0;
                        HUD.ShowMessage("Rage bonus ended.", ".", 1f);
                    }

                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    // %%% Handle Blood Room Event %%%%%%%%%%%%%%%%
                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    if (blood_room_event_active)
                    {


                        // increment seconds elapsed after processing
                        room_tracker.blood_room_event_seconds_elapsed++;

                        // increase player health
                        Player.Instance.Damageable.Heal(0.5f);


                    }

                    // disable blood regen bonus
                    if (room_tracker.blood_room_event_seconds_elapsed >= 9)
                    {
                        room_tracker.blood_room_event_seconds_elapsed = 0;
                        room_tracker.blood_room_event_active = false;
                    }


                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    // %%% Handle Greed Room Event %%%%%%%%%%%%%%%%
                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    if (greed_room_event_active)
                    {


                        // increment seconds elapsed after processing
                        room_tracker.greed_room_event_seconds_elapsed++;

                        // increase player coins
                        Player.Instance.Coins += 1;


                    }

                    // disable coin bonus
                    if (room_tracker.greed_room_event_seconds_elapsed >= 60)
                    {
                        room_tracker.greed_room_event_seconds_elapsed = 0;
                        room_tracker.greed_room_event_active = false;
                    }


                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    // %%% Handle Armory Room Event %%%%%%%%%%%%%%%
                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    if (armory_room_event_active)
                    {

                        // increment seconds elapsed after processing
                        room_tracker.armory_room_event_seconds_elapsed++;

                        // increase player bombs
                        Player.Instance.Bombs += 1;

                    }

                    // disable bomb bonus
                    if (room_tracker.armory_room_event_seconds_elapsed >= 25)
                    {
                        room_tracker.armory_room_event_seconds_elapsed = 0;
                        room_tracker.armory_room_event_active = false;
                    }

                }

                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%% Melee Mode Tick Bonuses %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


                // ensure melee mode is enabled before testing bonuses
                if (mod_cb.melee_mode_enabled)
                {


                    // no melee-mode bonuses can be applied while bleeding
                    if (mod_cb.current_save_game.Bleeding)
                        continue;

                    // number of hands weilding melee weapons
                    int hands_melee = 0;

                    // Check to see if a player has ranged weapons equipped, if they do
                    // do not give health regen bonus.
                    foreach (var hand in Player.Instance.Hands)
                    {

                        // if our hand has a weapon check to ensure it's not ranged
                        if (hand.CurrentWeapon != null)
                        {

                            // return if it's a ranged type (staff/bow), if not just break and do regen
                            switch (hand.CurrentWeapon.Type)
                            {
                                case WeaponType.ComitasStaff:
                                case WeaponType.DisiplinaBow:
                                case WeaponType.JustitiaStaff:
                                    break;
                                default:
                                    hands_melee++;
                                    break;
                            }

                        }

                    }

                    // if we are dual weilding give full bonus (but only up to 5 health)
                    if (Player.Instance.Damageable.Health <= 5)
                        if (hands_melee == 2)
                        {

                            // strength bonus is only active when dual weilding melee weapons
                            float strength_bonus = 0f;

                            // calculate melee mode strength bonus
                            if (mod_cb.current_save_game.Strength > 0)
                                strength_bonus = (mod_cb.current_save_game.Strength * 0.001f) / 2;

                            // Heal for a small amount since the player is a melee madman doing melee swordspam. 
                            // Cardio bonus for heart health!
                            Player.Instance.Damageable.Heal(0.10f + strength_bonus);

                        }

                    // only give 25% bonus if player is weilding ranged + melee
                    if (Player.Instance.Damageable.Health <= 10)
                        if (hands_melee == 1)
                        {

                            // heal for 1/4th the bonus if the player is ranged + melee , do not apply strength bonus
                            Player.Instance.Damageable.Heal(0.07f);

                        }
                    
                }

                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    // %%% Intelligence Shield Regen Tick Bonuses %%%%%%%%%%%%%%%%%%%
                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    // melee mode is a catch all for regen bonuses and is used here
                    // to indicate that regen bonuse are enabled (works for int)
                    if (mod_cb.melee_mode_enabled)
                    {

                        /* Shield Regen works if you're bleeding since it doesn't affect life.
                        // no melee-mode bonuses can be applied while bleeding
                        if (mod_cb.current_save_game.Bleeding)
                            continue;
                        */

                        // number of staves equipped
                        int hands_staff = 0;

                        // Check to see if a player has ranged weapons equipped, if they do
                        // do not give health regen bonus.
                        foreach (var hand in Player.Instance.Hands)
                        {

                            // if our hand has a weapon check to ensure it's not ranged
                            if (hand.CurrentWeapon != null)
                            {

                                // return if it's a ranged type (staff/bow), if not just break and do regen
                                switch (hand.CurrentWeapon.Type)
                                {
                                    case WeaponType.ComitasStaff:
                                    case WeaponType.JustitiaStaff:
                                        hands_staff++;
                                        break;
                                    default:
                                        break;
                                }

                            }

                        }

                        // if we have no hands melee, go ahead and add the shield regen bonus
                        if (hands_staff == 2)
                        {

                            // strength bonus is only active when dual weilding melee weapons
                            float shield_regen_bonus = 0f;

                            // calculate shield regen bonuses
                            if (mod_cb.current_save_game.Intelligence > 0)
                                shield_regen_bonus = (mod_cb.current_save_game.Intelligence * 0.001f);

                            // only divide bonus by four if we're dual weilding staves
                            shield_regen_bonus /= 4;

                            // add the shield bonus
                            Player.Instance.Damageable.AddShield(0.10f + shield_regen_bonus);

                        }
                        else if (hands_staff == 1)
                        {

                            // strength bonus is only active when dual weilding melee weapons
                            float shield_regen_bonus = 0f;

                            // calculate shield regen bonuses
                            if (mod_cb.current_save_game.Intelligence > 0)
                                shield_regen_bonus = (mod_cb.current_save_game.Intelligence * 0.001f);

                            // since we're dual weilding, divide bonus by double so it's not ridiculously OP
                            shield_regen_bonus /= 8;

                            // Increase shielding.
                            Player.Instance.Damageable.AddShield(0.10f + shield_regen_bonus);

                        }

                    }

                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    // %%% Apply Player Caps %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    // if we have a player instance to work with, adjust player caps (if overcap)
                    if (Player.Instance != null)
                    {

                        // enforce max health
                        if(Player.Instance.Damageable.MaxHealth > 12)
                        {

                                // set max health and health
                                Player.Instance.Damageable.MaxHealth = 12;

                                // heal player
                                Player.Instance.Damageable.Heal(12);

                                // show health cap message
                                HUD.ShowMessage("Health Cap Reached", "You have reached the games health cap, so you have been granted full life instead of extra health.", 6f);

                        }

                        // enforce shield cap
                        if(Player.Instance.Damageable.Shield > 5)
                        {

                            // set shield to 4
                            Player.Instance.Damageable.Shield = 5;
                            
                       }    
                    
                }

                // yield wait (this causes execution to be delayed one second)
                yield return new WaitForSeconds(1);

            }

        }

    }

}
