﻿using System;
using Legend;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Diagnostics;
using LegendOfLucaAdventureMod;

/*
 * 
 * ABOUT:  This mod turns the SteamVR/HTC Vive game Legend of Luca from a short roguelike, into a multi-hour (10+hr)
 *         dungeon crawler with persistent saves.  The mod performs level layout randomization by mixing and
 *         matching level designs.  Saves are done on room enter.
 *         
 *         The mod includes a melee-mode which enables a 1 heart every 10s life regeneration.  This doesn't seem like a lot
 *         but it dramatically increases player survivability.  You can uncomment and rebuild this mod if you want without
 *         it in, but I play melee as a workout, so there ya go.  The life regen is capped at 10 hearts though, so if you 
 *         regen slow, you'll still not be OP with full health.  You can increase this regen by increasing your strength 
 *         stat (use melee weapons only) which will allow you to increase it by 1/2heart per second at max.
 *         
 * 
 *         
 * INSTALL: Simply move the LegendOfLucaAdventureMod.dll in the release folder to your legend of luca mods folder, 
 * 
 *  -> ./steamapps/common/Legend of Luca/Mods/
 *  
 *  and copy the custom rooms into your rooms folder.  Should be all that's necessary. 
 *  
 *  -> ./steamapps/common/Legend of Luca/Rooms/
 *         
 * In the future I may encode the rooms and put them inside the binary itself so you don't have to 
 * install the rooms, but for now I'm keeping things separate as I'm likely to change some layouts.
 * 
 * 
 *         
 * BUILD:  You can build this source by downloading the project from bitbucket, and importing it intoo
 *         Visual Studio Community Edition (free).  Just click on the mods solution file after you download it 
 *         and everything should load OK.  If you are missing any references, you add them by going to:
 *         
 *         project -> add reference
 *         
 *         and browsing till you find the reference (dll file) you're looking for.  After you build this mod, a DLL
 *         artifact is produced.  Simply move that dll to your legend of luca mods folder and the mod will load during
 *         the next time you start the game.  This mod logs to the standard game logger.
 *          
 *         
 *         I should note that this is my first C# project, so if anyone notices any
 *         bugs, let me know.  Should be fine though (i think).
 * 
 * 
 * SAVE:  Save files are just JSON, so you can edit them however you want.  Open up your save file after you exit the game and
 *        you'll be able to add bombs/keys/coins etc.
 *        
 *        
 * CHEATS:   Just modify your save file with any editor you want.
 * 
 * 
 * LICENSE:  The code in this mod and the rooms that I've built (not the game itself) is licensed under 
 *           the MIT license and can be used however you want, in any way you want.  Steal it, reuse it, whatever.  
 *           It's fine.
 *           
 * THANK YOU: To Lonecoder1 for making an awesomely moddable game.  When I first made this mod, he could've
 *            been an asshole about it, but he wasn't.  Instead, he wrote a mod loader for me in the game itself, 
 *            helped me debug issues when they came up, and helped review mod code.
 *            
 *            He made private members public, gave me example code where necessary, and was all around patient.
 *            Kudos dawg.  Other developers should take note.
 *        
 *            
 * MODDERS: This game is very easy to mod, and you can create wonderful experiences with it.  I recommend
 *          you either use my mod as a template, or copy things from it to your liking.  
 *          
 * NOTES FOR LATER DEV:
 *     Playing audio from script files:
 *       http://answers.unity3d.com/questions/954062/play-audio-from-wav-file-from-script.html
 *            
 */


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%% Save Game Information %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// save game class (restored or stored from JSON)
public class AdventureModSaveGame
{

    // current playthrough
    public int Playthrough = 1;

    // last level number
    public int LevelNumber = 1;

    // players max health
    public float Maxhealth = 10;

    // players current health
    public float Health = 6;

    // players damage output
    public float Damage = 1;

    // players current bomb count
    public int Bombs = 20;

    // players current coin count
    public int Coins = 0;

    // players current key count
    public int Keys = 6;

    
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Counters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // number of enemies killed
    public int EnemyKillCount = 0;

    // number of rooms entered
    public int RoomEnterCount = 0;

    // set default kill count (not xp since we don't really have xp values yet)
    public int KillCountdownRemainingTillNextLevelUp = 10;


    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Status Effects %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // burning efffect (player loses life and some random non-weapon item)
    public bool Burning;
    public float BurnResist;
    public float BurnTimeRemaining;

    // poisoned effect (player loses life and shield at same rate)
    public bool Poisoned;
    public float PoisonResist;
    public float PoisonTimeRemaining;

    // bleeding effect (player loses health only)
    public bool Bleeding;
    public float BleedingResist;
    public float BleedingTimeRemaining;

    // clumsy effect (randomly drop item types)
    public bool Clumsy;
    public float ClumsyResist;
    public float ClumsyTimeRemaining;

    // blind
    public bool Blind;
    public float BlindResist;
    public float BlindTimeRemaining;



    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Stats %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // Raw damage multiplier which will be applied to melee wepaons, 
    // goes up after set number of melee kills.  This also increases
    // your max health and health regeneration.
    public float Strength;

    // Dexterity increases you critical strike chance, increases bow attack speed,
    // and also gives you a chance to evade damage.  This stat is increased by 
    // either using only one melee weapon, or a bow to kill enemies.
    public float Dexterity;

    // Intelligence allows for energy-shield regenration. (shield hearts)
    public float Intelligence;

    // Evasion is raised by weilding different weapon types (eg. int/dex weapons, str/int weapons) and damaging enemies.  It
    // causes you to start to build up the capability to dodge mob spells and attacks
    public float Evasion;


    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Special Bonuses %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // Lucky rooms give extra flat crit chance that isn't affected
    // by dexterity.  This lets us track that.
    public float LuckyCritOvercap;

    // This is the rage bonus that is activated when you enter a rage
    // room.
    public float RageBonus;

    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Items %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // item ids currently posessed
    public List<ItemId> ItemIds;


    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Weapons %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // weapon ids currently posessed
    public List<WeaponType> WeaponIds;
    
};

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%% Mod Class Implementation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


public class LegendOfLucaAdventureMode : IMod
{

    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Global Mod Singleton %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // create self reference
    public static LegendOfLucaAdventureMode Instance;


    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Mod Basic Information %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // set mod name
    public string Name { get { return "Legend of Luca Adventure Mod"; } }

    // set your mod description here
    public string Description { get { return "Legend of Luca Adventure Mod"; } }

    // set your mod author here
    public string Author { get { return "Dominic McGillicutty"; } }

    // set your mod version here
    public int Version { get { return 1; } }

    // returns debug
    public int DebugMode { get { return 1; } }

    // default save file for this mod
    public string DefaultSaveFile = null;




    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Room Trackers %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
   
    // this is used to track room travel
    public AdventureModRoomTracker room_tracker;

    // attempt to enter a room
    public void OnRoomEnter(Room room)
    {

        // insure level instance isn't null
        if (Level.Instance == null)
        {
            this.ModLogger("ERROR", "Attempting to spawn player into a null level instance.");
            return;
        }

        // switch on the level number and return if we're on a special level (not game-level)
        switch(Level.Instance.LevelNumber)
        {
                case 0:
                    return;
                case 6:
                    return;
                case 7:
                    return;
                default:
                    break;
        }
        

        // if the room tracker 
        if (room_tracker == null)
            room_tracker = new AdventureModRoomTracker();

        // attempt to trigger our room enter callback
        this.room_tracker.OnRoomEnter(room, Player.Instance, Level.Instance, this);

    }

    // on item pickup
    public void OnPickup(Pickup pickup)
    {

    }

    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Player Hit Tracker %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // create player tracker
    public AdventureModPlayerTracker player_tracker;

    // tracks player being hit
    public OnPlayerHitResult OnPlayerHit(MonoBehaviour source)
    {

        // create player tracker if one isn't already created
        if(this.player_tracker == null)
        {
            this.player_tracker = new AdventureModPlayerTracker();
        }

        // return the player
        return this.player_tracker.OnPlayerHit(source, this);
        
    }


    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Enemy Trackers %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // this is used to track enemy information
    public AdventureModEnemyTracker enemy_tracker;

    // invoke enemy tracker damage tracker when attack callback triggers
    public void OnEnemyDamaged(Damageable damageable, MonoBehaviour source)
    {
        if (this.enemy_tracker == null)
            this.enemy_tracker = new AdventureModEnemyTracker();

        // set outside callback
        this.enemy_tracker.OnEnemyDamaged(damageable, source, this);

    }

    // increment the enemy death count
    public void OnEnemyDie(Enemy enemy)
    {
        
        // track enemy death (and apply savageness bonuses)
        this.enemy_tracker.OnEnemyDie(enemy, this);

    }


    

    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Logging Facility %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // Simply logs a message to the debug file found in the managed data directory.  You can
    // tail the file to view your log output at runtime.  It will show you stack traces if your
    // application fails as well, so be sure to check the log file if you're having problems.
    public void ModLogger(String report_level, String report_message)
    {
        
        // create debug message
        string debug_msg = String.Format("{0}:{1}:{2}", "AdventureModLogger", report_level, report_message);

        // log to file
        UnityEngine.Debug.Log(debug_msg);

    }

    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Player OnLoad Hook %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // mod playthrough (set via save)
    public int Playthrough;
    
    // set on level load to the current save game available
    public AdventureModSaveGame current_save_game;

    // folder on the filesystem (passed in from OnLoad)
    public string ModFilesystemFolder;

    // rooms filesystem folder
    public string RoomsFilesystemFolder;

    // audio filesystem folder (wav files)
    public string WavFilesystemFolder;

    // mod audio 
    public AdventureModAudio mod_audio;

    // timer to use for starting the mod
    AdventureModTimer mod_timer;

    // onload 
    public void OnLoad(string mod_folder)
    {
        
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Set Singleton and Combine Paths %%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // set singleton to null OnLoad (prevents indirect use)
        LegendOfLucaAdventureMode.Instance = (LegendOfLucaAdventureMode)null;

        // set mod folder from passed in arg
        this.ModFilesystemFolder = mod_folder;

        // attempt to combine the filesystem path to find our custom rooms
        this.RoomsFilesystemFolder = Path.Combine(this.ModFilesystemFolder, "AdventureModRooms\\");

        // add log message indicating our load attempt
        string mod_base_fmt = string.Format("Using mod base directory {0}.  Using mod rooms directory {1}", this.ModFilesystemFolder, this.RoomsFilesystemFolder);
        this.ModLogger("NOTICE", mod_base_fmt);
        

        // ensure the rooms filesystem folder exists (mod folder should always be a good value since it's passed
        // in from steam)
        if (!Directory.Exists(this.RoomsFilesystemFolder))
        {
            string room_path_error = String.Format("Rooms path not found on the filesystem: {0}.  This is a mod-developer problem, not your fault, blame the mod author.");
            this.ModLogger("ERROR", room_path_error);
            return;
        }

        
        // create default save file
        string save_file = Path.Combine(this.ModFilesystemFolder, "AdventureMod.savestate");

        // set default save file
        this.DefaultSaveFile = save_file;
        
        // log save file
        this.ModLogger("NOTICE", this.DefaultSaveFile);

        

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Pre-Load All Rooms from Filesystem %%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // Due to game changes, this needs to be done here first before modifications
        // below.  Only add 1-5, specifically those.  0 is the starting level you show up 
        // in when you start the game, 6 and 7 are reserved.
        int design_iter = 1;
        for(; design_iter <= 5; design_iter++)
        {

            // add standard levels
            Level.Instance.Designs[design_iter].RoomFilenames.Add(Path.Combine(this.RoomsFilesystemFolder, "All Levels.tmx"));
            Level.Instance.Designs[design_iter].RoomFilenames.Add(Path.Combine(this.RoomsFilesystemFolder, "Level 1.tmx"));
            Level.Instance.Designs[design_iter].RoomFilenames.Add(Path.Combine(this.RoomsFilesystemFolder, "Level 2.tmx"));
            Level.Instance.Designs[design_iter].RoomFilenames.Add(Path.Combine(this.RoomsFilesystemFolder, "Level 3.tmx"));
            Level.Instance.Designs[design_iter].RoomFilenames.Add(Path.Combine(this.RoomsFilesystemFolder, "Level 4.tmx"));
            Level.Instance.Designs[design_iter].RoomFilenames.Add(Path.Combine(this.RoomsFilesystemFolder, "Level 5.tmx"));

            // add adventure mod custom levels
            Level.Instance.Designs[design_iter].RoomFilenames.Add(Path.Combine(this.RoomsFilesystemFolder, "UnescapableHallsLevel1.tmx"));
            Level.Instance.Designs[design_iter].RoomFilenames.Add(Path.Combine(this.RoomsFilesystemFolder, "UnescapableHallsLevel2.tmx"));
            Level.Instance.Designs[design_iter].RoomFilenames.Add(Path.Combine(this.RoomsFilesystemFolder, "UnescapableHallsLevel3.tmx"));
            Level.Instance.Designs[design_iter].RoomFilenames.Add(Path.Combine(this.RoomsFilesystemFolder, "UnescapableHallsLevel4.tmx"));
            Level.Instance.Designs[design_iter].RoomFilenames.Add(Path.Combine(this.RoomsFilesystemFolder, "UnescapableHallsLevel5.tmx"));
            
        }

        // set default initial playthrough to 1
        this.Playthrough = 1;

        // log save file
        this.ModLogger("NOTICE", "Finished loading ok!");

        // set global singleton after all values are set
        LegendOfLucaAdventureMode.Instance = (LegendOfLucaAdventureMode)this;


        /**/

    }

    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Player Spawn Load Hook (Called When Player Spawns %%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    // called when player spawns into the game (start of level, game load)
    public void OnPlayerSpawn(Player player)
    {
        // Note: this is no longer used.  It once contained a significant amount of code
        //       but after discussion with the game author it makes more sense simply
        //       to put code elsewhere (mainly other callbacks)
        
    }


    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Level Loading Callback %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // Note:  In this mod, this callback is used to generate custom random levels with 
    //        randomized tiles.  It is very important that you pay attention to the
    //        code below, as it's responsible for generating the levels you'll be playing.


    // custom level loader
    public void OnGotoLevel(LevelDesign level)
    {
        
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Level Generator Prologue %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // log level generation attempt
        string level_generation_notice = String.Format("Level {0} is attempting generation.", Level.Instance.LevelNumber);
        this.ModLogger("NOTICE", level_generation_notice);
                
        // Never work on levels 0, 6, or 7.  Mod only works on levels 1-5.
        switch(Level.Instance.LevelNumber)
        {

            // exit immediately if level is unrecognized/bad
            case 0:
            case 6:
                return;
                
            // only give playthrough bonus on achieving level 7
            case 7:

                // if the player beats playthrough 9, let them know it
                if (this.current_save_game.Playthrough >= 9)
                {
                    HUD.ShowMessage("Playthrough > 9 beat?  Damn dude.", "If you got this message legitimately, you've likely swung a virtual sword thousands of times at imaginary monsters in your living room.  Probably burned many thousands of calories too.   Future is great isn't it!  Now download the source to this mod and get hacking so that we can get more vive content for all!", 8f);
                }
                else
                {
                    HUD.ShowMessage("CONGRATS!", "Now on to the next playthrough.  Monster difficulty will increase up until playthrough 9.  Beneficial special room frequency will also increase (more armory, greed, blood rooms) by 1% on each difficulty.", 8f);
                    this.current_save_game.Playthrough++;
                }

                // saving the mod state from within level 7 resets the level to level 1.
                this.SaveModGameState(this.DefaultSaveFile, this.current_save_game);
                this.LoadModGameState(this.DefaultSaveFile);
                return;

            default:
                break;

        }
         

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Load Default Sounds %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // set wav filesystem folder
        this.WavFilesystemFolder = Path.Combine(this.ModFilesystemFolder, "AdventureModSounds\\");

        // create new mod audio as game object (must be game object)
        this.mod_audio = Level.Instance.gameObject.AddComponent<AdventureModAudio>();

        // load default mod sounds (for later playback)
        this.mod_audio.LoadModDefaultSounds(this);



        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Start Mod Global Ticking Timer %%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // start the mod timer (only once)
        if(this.mod_timer == null)
        { 

            // create new mod timer
            this.mod_timer = Level.Instance.gameObject.AddComponent<AdventureModTimer>();

            // start the mod timer
            this.mod_timer.StartGlobalTimer();

        }

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Load Game from Save %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // load state if it exists
        if (this.current_save_game == null)
        { 

            if (!this.LoadModGameState(this.DefaultSaveFile))
            {

                // set save game to new save game if load fails for whatever reason
                this.current_save_game = new AdventureModSaveGame();

                // attempt to create state if no state exists
                this.SaveModGameState(this.DefaultSaveFile, this.current_save_game);

                // attempt to load from the new save (sets class member for future save data)
                this.LoadModGameState(this.DefaultSaveFile);
                
            }

        }
        else
        {
            this.SaveModGameState(this.DefaultSaveFile, this.current_save_game);
            this.LoadModGameState(this.DefaultSaveFile);
        }

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Check To See if We're at an Exit Position %%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        /*
        // switch on the level number
        switch(Level.Instance.LevelNumber)
        {

            // exit to main room on any level 6-7 requests (should only have level 6)
            case 6:
            case 7:
                this.current_save_game.LevelNumber = 1;
                this.SaveModGameState(this.DefaultSaveFile, this.current_save_game);
                this.LoadModGameState(this.DefaultSaveFile);
                Level.Instance.GotoLevel(1);
                return;

            // simply break on regular levels
            default:
                break;
        }
        */

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Melee Mode %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // you can enable melee mode here if you want (adds regen timers
        // for your player so you can actually survive melee.  Regens go
        // up based on playthrough).  Only needs to be called once during player spawn.
        this.EnableMeleeMode();

        // clear the level on startup
        Level.Instance.ClearLevel();
                
        // log level generation attempt
        level_generation_notice = String.Format("Level {0} is has passed initial generation checks.", Level.Instance.LevelNumber);
        this.ModLogger("NOTICE", level_generation_notice);
               

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Save Game Level Movement (If Necessary) %%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // go to level number if loaded level is not the same as our current level
        if (this.current_save_game != null)
        {
            
            // if we're on the wrong level, move to the correct one
            if (Level.Instance.LevelNumber != this.current_save_game.LevelNumber)
            { 

                // set the instance level number before moving levels
                Level.Instance.LevelNumber = this.current_save_game.LevelNumber;

                // move levels
                Level.Instance.GotoLevel(this.current_save_game.LevelNumber);

                // exit here to prevent overlap (recurses)
                return;

            }

        }



        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Level Generation Multipliers %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // Note: All multipliers are set on a per-level basis in the if{} statements below.

        // normal room multiplier
        float normal_room_multiplier = 0f;

        // pseudo random room multiplier
        float pseudo_random_room_multiplier = 0f;

        // shop room multiplier
        float shop_room_multiplier = 0f;

        // hidden room multiplier
        float hidden_room_multiplier = 0f;

        // default treasure room multiplier
        float treasure_room_multiplier = 0f;

        // the number of bogus boss rooms (there is only one actual boss room)
        float bogus_boss_room_multiplier = 0f;

        // the number of weapon rooms
        float extra_weapon_room_multiplier = 0f;

        // number of extra "event" rooms (not to be confused with this mod's special rooms, event rooms
        // are rooms where you see gambling stuff etc)
        float extra_event_room_multiplier = 0f;



        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Level Adjustments %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // NOTE: In the section below you can edit parameters for any "level" 
        //       being loaded.  I put them all here in one section so it will
        //       be easy for you to affect your mods drastically, with only
        //       small changes in a few lines of code.  Happy modding!
        

        // setup randomizers (for adding to multiplier ranges)
        System.Random normal_room_randomizer        = new System.Random();
        System.Random pseudo_random_room_randomizer = new System.Random();
        System.Random shop_room_randomizer          = new System.Random();
        System.Random hidden_room_randomizer        = new System.Random();
        System.Random treasure_room_randomizer      = new System.Random();
        System.Random bogus_boss_room_randomizer    = new System.Random();
        System.Random extra_weapon_room_randomizer  = new System.Random();
        System.Random event_room_randomizer         = new System.Random();
        

        // set the instance 
        Level mod_level_instance = Level.Instance;

        // If youset this to true, the algorithims below will use test values to generate small
        // levels that can be quickly completed to verify that things are working as expected.
        bool use_test_multipliers_for_level_generation_debugging = false;


        // perform arbitrary adjustments for level 1
        if (mod_level_instance.LevelNumber == 1)
        {

            
            String display_msg = String.Format("Death and despair, this is what you know. With that knowledge you begin your long quest to overcome the horrors which exist within this terrible labyrinth.");
            display_msg += "\n\n Adventure Mod Playthrough Number: " + this.current_save_game.Playthrough;
                
            

            // show the load message
            HUD.ShowMessage("Act 1:", display_msg, 12);


            // create level 1 multipliers (random values)
            normal_room_multiplier = normal_room_randomizer.Next(70, 90);
            pseudo_random_room_multiplier = pseudo_random_room_randomizer.Next(70, 90);
            shop_room_multiplier = shop_room_randomizer.Next(4, 9);
            hidden_room_multiplier = hidden_room_randomizer.Next(4, 9);
            treasure_room_multiplier = treasure_room_randomizer.Next(4, 9);
            bogus_boss_room_multiplier = bogus_boss_room_randomizer.Next(4, 9);
            extra_weapon_room_multiplier = extra_weapon_room_randomizer.Next(4, 9);
            extra_event_room_multiplier = event_room_randomizer.Next(4, 9);

            // override with test multipliers if desired
            if(use_test_multipliers_for_level_generation_debugging == true)
            { 
                // testing multipliers (for testing level generation)
                normal_room_multiplier = 2;
                pseudo_random_room_multiplier = 2;
                shop_room_multiplier = 2;
                hidden_room_multiplier = 2;
                treasure_room_multiplier = 1;
                bogus_boss_room_multiplier = 1;
                extra_weapon_room_multiplier = 1;
                extra_event_room_multiplier = 1;
            }


        }

        // perform arbitrary adjustments for level 2 (multiply all values by 2;
        if (mod_level_instance.LevelNumber == 2)
        {

            // create level number for display 
            String display_msg = String.Format("As the damned soul rises, so does the fire and as the doors of hell open a soul sinks deeper. (difficulty has been increased)");
            display_msg += "\n\n Adventure Mod Playthrough Number: " + this.current_save_game.Playthrough;

            // show the load message
            HUD.ShowMessage("Act 2:", display_msg, 12);

            // reduce player damage by 10% of total
            Player.Instance.Damage *= 0.90f;

            // create level 2 multipliers (random values)
            normal_room_multiplier        = normal_room_randomizer.Next(70, 100);
            pseudo_random_room_multiplier = pseudo_random_room_randomizer.Next(70, 100);
            shop_room_multiplier          = shop_room_randomizer.Next(4, 11);
            hidden_room_multiplier        = hidden_room_randomizer.Next(4, 11);
            treasure_room_multiplier      = treasure_room_randomizer.Next(4, 11);
            bogus_boss_room_multiplier    = bogus_boss_room_randomizer.Next(4, 11);
            extra_weapon_room_multiplier  = extra_weapon_room_randomizer.Next(4, 11);
            extra_event_room_multiplier = event_room_randomizer.Next(5, 7);

            // override with test multipliers if desired
            if (use_test_multipliers_for_level_generation_debugging == true)
            {
                // testing multipliers (for testing level generation)
                normal_room_multiplier = 2;
                pseudo_random_room_multiplier = 2;
                shop_room_multiplier = 2;
                hidden_room_multiplier = 2;
                treasure_room_multiplier = 1;
                bogus_boss_room_multiplier = 1;
                extra_weapon_room_multiplier = 1;
                extra_event_room_multiplier = 1;
            }

        }

        // perform arbitrary adjustments for level 3
        if (mod_level_instance.LevelNumber == 3)
        {

            // create level number for display 
            String display_msg = String.Format("The only familiar thing here are the walking malicious fates of a many long forgotten agonies.  The creatures here mmanifest sorrow through mutilation, and express vanity through abomination.  They will break you, or you will break them, this is hells bargain. (difficulty has been increased further)");
            display_msg += "\n\n Adventure Mod Playthrough Number: " + this.current_save_game.Playthrough;

            // show the load message
            HUD.ShowMessage("Act 3:", display_msg, 12);

            // reduce player damage by 10% of total
            Player.Instance.Damage *= 0.90f;

            // create level 3 multipliers (random values)
            normal_room_multiplier        = normal_room_randomizer.Next(80, 120);
            pseudo_random_room_multiplier = pseudo_random_room_randomizer.Next(80, 120);
            shop_room_multiplier          = shop_room_randomizer.Next(5, 13);
            hidden_room_multiplier        = hidden_room_randomizer.Next(5, 13);
            treasure_room_multiplier      = treasure_room_randomizer.Next(5, 13);
            bogus_boss_room_multiplier    = bogus_boss_room_randomizer.Next(5, 13);
            extra_weapon_room_multiplier  = extra_weapon_room_randomizer.Next(5, 13);
            extra_event_room_multiplier = event_room_randomizer.Next(5, 7);

            // override with test multipliers if desired
            if (use_test_multipliers_for_level_generation_debugging == true)
            {
                // testing multipliers (for testing level generation)
                normal_room_multiplier = 2;
                pseudo_random_room_multiplier = 2;
                shop_room_multiplier = 2;
                hidden_room_multiplier = 2;
                treasure_room_multiplier = 1;
                bogus_boss_room_multiplier = 1;
                extra_weapon_room_multiplier = 1;
                extra_event_room_multiplier = 1;
            }
        }

        // perform arbitrary adjustments for level 4
        if (mod_level_instance.LevelNumber == 4)
        {

            // create level number for display 
            String display_msg = String.Format("Sulfur burns your lungs as you press on.  You contemplate turning around.  You ask yourself if it's worth it, and you remind yourself over the sound of yoru body beginning to shut down from wear, you remind yourself... you barely remember but you make out a memory. (difficulty has been increased further)");
            display_msg += "\n\n Adventure Mod Playthrough Number: " + this.current_save_game.Playthrough;

            // show the load message
            HUD.ShowMessage("Act 4:", display_msg, 12);

            // reduce player damage by 10% of total
            Player.Instance.Damage *= 0.90f;

            // create level 4 multipliers (random values)
            normal_room_multiplier        = normal_room_randomizer.Next(90, 130);
            pseudo_random_room_multiplier = pseudo_random_room_randomizer.Next(80, 130);
            shop_room_multiplier          = shop_room_randomizer.Next(6, 16);
            hidden_room_multiplier        = hidden_room_randomizer.Next(6, 16);
            treasure_room_multiplier      = treasure_room_randomizer.Next(6, 16);
            bogus_boss_room_multiplier    = bogus_boss_room_randomizer.Next(6, 16);
            extra_weapon_room_multiplier  = extra_weapon_room_randomizer.Next(6, 16);
            extra_event_room_multiplier   = event_room_randomizer.Next(5, 7);

            // override with test multipliers if desired
            if (use_test_multipliers_for_level_generation_debugging == true)
            {
                // testing multipliers (for testing level generation)
                normal_room_multiplier = 2;
                pseudo_random_room_multiplier = 2;
                shop_room_multiplier = 2;
                hidden_room_multiplier = 2;
                treasure_room_multiplier = 1;
                bogus_boss_room_multiplier = 1;
                extra_weapon_room_multiplier = 1;
                extra_event_room_multiplier = 1;
            }

        }

        // perform arbitrary adjustments for level 5
        if (mod_level_instance.LevelNumber == 5)
        {
            
            // create level number for display 
            String display_msg = String.Format("As you reach the final depth of the labyrinth,  you remember the people you are fighting for.  You remember that this fight was not one you started.  You remember all those who died, those who suffered, all those who bled out at the merciless cruelty of such basal evils.  You begin your final journey to destroy them once and for all.  (difficulty has been increased further)");
            display_msg += "\n\n Adventure Mod Playthrough Number: " + this.current_save_game.Playthrough;

            // show the load message
            HUD.ShowMessage("Act 5:", display_msg, 12);

            // reduce player damage by 10% of total
            Player.Instance.Damage *= 0.90f;
            
            // add level 5 multipliers
            normal_room_multiplier        = normal_room_randomizer.Next(120, 150);
            pseudo_random_room_multiplier = pseudo_random_room_randomizer.Next(120, 150);
            shop_room_multiplier          = shop_room_randomizer.Next(8, 20);
            hidden_room_multiplier        = hidden_room_randomizer.Next(8, 20);
            treasure_room_multiplier      = treasure_room_randomizer.Next(8, 20);
            bogus_boss_room_multiplier    = bogus_boss_room_randomizer.Next(8, 20);
            extra_weapon_room_multiplier  = extra_weapon_room_randomizer.Next(8, 20);
            extra_event_room_multiplier   = event_room_randomizer.Next(5, 7);

            // override with test multipliers if desired
            if (use_test_multipliers_for_level_generation_debugging == true)
            {
                // testing multipliers (for testing level generation)
                normal_room_multiplier = 2;
                pseudo_random_room_multiplier = 2;
                shop_room_multiplier = 2;
                hidden_room_multiplier = 2;
                treasure_room_multiplier = 1;
                bogus_boss_room_multiplier = 1;
                extra_weapon_room_multiplier = 1;
                extra_event_room_multiplier = 1;
            }

        }
        

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Level Generation Loop %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


        // NOTE: This try except creates a "loop".  If level generation fails (and it does fail, this is normal) 
        //       the loop will attempt to create the level again by calling SetLevel in the except statement
        //       at the bottom. Levels will fail generation when the generation engine "paints itself into a corner."  
        //       This is normal behavior and should be expected.  Very large levels can take a few times to generate. 
        //       That said, level generation is very quick and the user really shouldn't notice much if the loop
        //       iterates a few times.

        // attempt level generation
        try
        {


            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Step 1) Create "Normal" Rooms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // set spawn room
            mod_level_instance.SpawnRoom = mod_level_instance.CreateRoom(Level.CenterRoomsX, Level.CenterRoomsY, RoomType.Start);

            // set current room
            var currentRoom = mod_level_instance.SpawnRoom;
            Level.Instance.SetupRoom(currentRoom);

            // create "normal" rooms first (using random direction)
            for
            (
                int i = 0;
                i < normal_room_multiplier;
                i++
            )
            {

                // add debug message
                this.ModLogger("NOTICE", "Spawning 'normal' room.");

                // add normal room in random direction
                currentRoom = mod_level_instance.AddRoom(currentRoom, Level.RandomDirection());
                Level.Instance.SetupRoom(currentRoom);

            }

            // add debug message
            this.ModLogger("NOTICE", "Finished spawning 'normal' room.");

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Extra Event Rooms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // set current room
            currentRoom = mod_level_instance.SpawnRoom;

            // create pseudorandom layout
            for
            (
                int i = 0;
                i < extra_event_room_multiplier;
                i++
            )
            {

                // add debug message
                this.ModLogger("NOTICE", "Spawning 'event' room.");

                // add new event type room
                currentRoom = mod_level_instance.AddNewRoom(RoomType.Event);
                Level.Instance.SetupRoom(currentRoom);

            }


            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Some Pseudo-Random Rooms (Adds Depth to Layouts) %%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // set current room
            currentRoom = mod_level_instance.SpawnRoom;

            // create pseudorandom layout
            for
            (
                int i = 0;
                i < pseudo_random_room_multiplier;
                i++
            )
            {

                // add debug message
                this.ModLogger("NOTICE", "Spawning 'pseudo random' room.");
                
                // add pr room
                currentRoom = mod_level_instance.AddRoom(currentRoom, mod_level_instance.PsudoRandomDirection(currentRoom));
                Level.Instance.SetupRoom(currentRoom);

            }

            // add debug message
            this.ModLogger("NOTICE", "Finished spawning 'pseudo random' rooms.");

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Some Hidden Rooms (Adds Depth to Layouts) %%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // set current room
            currentRoom = mod_level_instance.SpawnRoom;

            // create pseudorandom layout
            for
            (
                int i = 0;
                i < hidden_room_multiplier;
                i++
            )
            {

                // add debug message
                this.ModLogger("NOTICE", "Spawning 'hidden' room.");

                // add hidden room
                currentRoom = mod_level_instance.AddNewRoom(RoomType.Hidden);
                Level.Instance.SetupRoom(currentRoom);

            }

            // add debug message
            this.ModLogger("NOTICE", "Finished spawning 'hidden' rooms.");


            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Some Treasure Rooms (Adds Depth to Layouts) %%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // set current room
            currentRoom = mod_level_instance.SpawnRoom;

            // create pseudorandom layout
            for
            (
                int i = 0;
                i < treasure_room_multiplier;
                i++
            )
            {

                // add debug message
                this.ModLogger("NOTICE", "Spawning 'treasure' room.");

                // add treasure room
                currentRoom = mod_level_instance.AddNewRoom(RoomType.Treasure);
                Level.Instance.SetupRoom(currentRoom);

            }

            // add debug message
            this.ModLogger("NOTICE", "Finished spawning 'treasure' rooms.");

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Some Shop Rooms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // set current room
            currentRoom = mod_level_instance.SpawnRoom;

            // create pseudorandom layout
            for
            (
                int i = 0;
                i < shop_room_multiplier;
                i++
            )
            {

                // add debug message
                this.ModLogger("NOTICE", "Spawning 'shop' room.");

                // add shop room
                currentRoom = mod_level_instance.AddNewRoom(RoomType.Shop);
                Level.Instance.SetupRoom(currentRoom);

            }

            // add debug message
            this.ModLogger("NOTICE", "Finished spawning 'shop' rooms.");



            

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%% Link up Rooms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // Link up rooms (this needs to be done before weapon rooms/bosses I think or it could
            // possibly create messed up levels.
            mod_level_instance.LinkAllNormalRooms();


            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Some Bogus Bosses for Extra Loot %%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // create bogus bosses layout
            for
            (
                int i = 0;
                i < bogus_boss_room_multiplier;
                i++
            )
            {
                currentRoom = mod_level_instance.AddNewRoom(RoomType.Boss);
                Level.Instance.SetupRoom(currentRoom);
            }



            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Extra Weapon Rooms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // create pseudorandom layout
            for
            (
                int i = 0;
                i < extra_weapon_room_multiplier;
                i++
            )
            {
                currentRoom = mod_level_instance.AddNewRoom(RoomType.Weapon);
                Level.Instance.SetupRoom(currentRoom);
            }

           


            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Add Final Boss Room %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // add debug message
            this.ModLogger("NOTICE", "Attempting to lookup random room that is not our spawn room.");

            // this loop prevents ever linking a start room to a boss room
            while (currentRoom == mod_level_instance.SpawnRoom)
            {
                currentRoom = mod_level_instance.GetRandomNormalRoom();
                Level.Instance.SetupRoom(currentRoom);
            }

            // set lookup succeeded notice
            this.ModLogger("NOTICE", "Lookup succeeded.");

            // add boss room
            currentRoom = mod_level_instance.AddNewRoom(RoomType.Boss, currentRoom);

            // set lookup succeeded notice
            this.ModLogger("NOTICE", "Final boss room has been added.");


            // attempt to link to level if we have a next level
            if (mod_level_instance.LevelNumber != 5)
            {

                // set lookup succeeded notice
                this.ModLogger("NOTICE", "Adding final weapon room.");

                // set current room
                currentRoom = mod_level_instance.AddNewRoom(RoomType.Weapon, currentRoom);
                Level.Instance.SetupRoom(currentRoom);

                // set lookup succeeded notice
                this.ModLogger("NOTICE", "Final weapon room has been added.");

                // link the new weapon room to the next level (level + 1)
                mod_level_instance.LinkRoomToLevel
                (
                        currentRoom,
                        mod_level_instance.GetAvailableDirection(currentRoom),
                        mod_level_instance.LevelNumber + 1
                );

                // set lookup succeeded notice
                this.ModLogger("NOTICE", "Final weapon room has been linked to next level.");

            }
            else
            {

                // if you beat the game link to victory level
                mod_level_instance.LinkRoomToLevel
                (
                    currentRoom,
                    mod_level_instance.GetAvailableDirection(currentRoom),
                    LevelDesign.VictoryLevelNumber
                );

            }


            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Simple Loop To Log Debug Output Re: Rooms %%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // toggle this to true if you want the debug output to contain
            // a list of rooms in order of generation.
            bool room_layout_debug = false;

            // run room generation output
            if (room_layout_debug == true)
            {
                foreach (var room in Level.Instance.Rooms)
                {

                    this.ModLogger("ROOMDBG", room.name);

                }
            }
         
        }
        catch (Exception e)
        {
            
            this.ModLogger("NOTICE", "Level generation failed.  Attempting to regenerate map again (this is normal).");
            mod_level_instance.ClearLevel();
            level.SetLevel();
            return;

        }
        
        // exit 
        return;

    }

    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Level Loaders %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // thanks to Lonecoder1, all the old level design code can be scrapped and
    // we can call LoadRoomDesigns directly! 
    public bool LoadRoomDesignIntoLevelFromFile(LevelDesign level_design_to_add_to, string filesystem_path_to_tmx, bool overwrite_existing_designs = false)
    {


        // ensure the file exists
        if (!File.Exists(filesystem_path_to_tmx))
        {

            // create formatted string for error reporting (let user know that they failed
            // a room load)
            string err_msg = String.Format("Adventure mod attempted to load custom level but it doesn't exist!: {0}", filesystem_path_to_tmx);
            HUD.ShowMessage("MODERRROR", err_msg);
            this.ModLogger("MODERROR", err_msg);
            return false;
        }

        // if the user wants to overwrite existing designs, do it here
        if (overwrite_existing_designs == true)
        { 

            // overwrite all the existing 
            level_design_to_add_to.RoomDesigns = null;

        }
        // attempt to load the room designs into our current level
        level_design_to_add_to.LoadRoomDesigns(filesystem_path_to_tmx);

        // always return true, need to check if levels were loaded later
        return true;

    }


    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Load/Save Game Game Methods %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // load mod game state
    public bool LoadModGameState(String load_file)
    {

        // ensure the save game exists
        if (!File.Exists(load_file))
        {
            this.ModLogger("WARNING", "Unable to load game save.");
            return false;
        }

        // load json save data
        var json_save_data = System.IO.File.ReadAllText(load_file);

        // load json data as a new object
        AdventureModSaveGame save_game_object =  JsonUtility.FromJson<AdventureModSaveGame>(json_save_data);


        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Reset Status Effects %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // burning efffect (player loses life and some random non-weapon item)
        save_game_object.Burning = false;
        save_game_object.BurnTimeRemaining = 0;

        // poisoned effect (player loses life and shield at same rate)
        save_game_object.Poisoned = false;
        save_game_object.PoisonTimeRemaining = 0;

        // bleeding effect (player loses health only)
        save_game_object.Bleeding = false;
        save_game_object.BleedingTimeRemaining = 0;

        // clumsy effect (randomly drop item types)
        save_game_object.Clumsy = false;
        save_game_object.ClumsyTimeRemaining = 0;

        // blind
        save_game_object.Blind = false;
        save_game_object.BlindTimeRemaining = 0;


        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Load Player Attributes %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // set player bombs
        if (save_game_object.Bombs > 0)
            Player.Instance.Bombs = save_game_object.Bombs;
        else
            Player.Instance.Bombs = 5;

        // set player keys
        if (save_game_object.Keys > 0)
            Player.Instance.Keys = save_game_object.Keys;
        else
            Player.Instance.Keys = 3;


        // set player coins
        if (save_game_object.Coins > 0)
            Player.Instance.Coins = save_game_object.Coins;
        else
            Player.Instance.Coins = 0;

        // current playthrough
        if (save_game_object.Playthrough > 0)
            this.Playthrough = save_game_object.Playthrough;
        else
            this.Playthrough = 1;

        // set max health
        if (save_game_object.Maxhealth > 0)
            Player.Instance.Damageable.MaxHealth = save_game_object.Maxhealth;
        else
            Player.Instance.Damageable.MaxHealth = 10;

        // Player can have more than max health during playthrough, but we never
        // allow that to compound.

        // set health
        if (save_game_object.Health > 6)
        { 
            Player.Instance.Damageable.Health = save_game_object.Health;
        }
        else
        {
            Player.Instance.Damageable.Health = 6;
        }

       

        // set player damage based on playthrough
        switch (save_game_object.Playthrough)
        {
            // 100% damage
            case 1:
                Player.Instance.Damage = 1.0f;
                break;
            // 90% damage
            case 2:
                Player.Instance.Damage = 0.90f;
                break;
            // 80% damage
            case 3:
                Player.Instance.Damage = 0.80f;
                break;
            // 70% damage
            case 4:
                Player.Instance.Damage = 0.70f;
                break;
            // 60% damage
            case 5:
                Player.Instance.Damage = 0.60f;
                break;
            // 50% damage
            case 6:
                Player.Instance.Damage = 0.50f;
                break;
            // 40% damage
            case 7:
                Player.Instance.Damage = 0.40f;
                break;
            // 30% damage
            case 8:
                Player.Instance.Damage = 0.30f;
                break;
            // 20% damage
            case 9:
                Player.Instance.Damage = 0.20f;
                break;
            // 10% damage
            case 10:
                Player.Instance.Damage = 0.10f;
                break;

            // default is 10% (anything past level 10)
            default:
                Player.Instance.Damage = 0.10f;
                break;
        }

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Check for Stat Caps %%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if (save_game_object.Intelligence >= 1000)
        {
            save_game_object.Intelligence = 1000;
        }
        if (save_game_object.Dexterity >= 1000)
        {
            save_game_object.Dexterity = 1000;
        }
        if (save_game_object.Strength >= 1000)
        {
            save_game_object.Strength = 1000;
        }
        if (save_game_object.Evasion >= 1000)
        {
            save_game_object.Evasion = 1000;
        }



        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Add Dexterity Speed Bonus %%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if (save_game_object.Dexterity > 0)
        {

            // gather attack speed bonus (500% speed increase max, reduce later after we can crit)
            float dex_attack_speed_bonus = ((save_game_object.Dexterity * 0.01f) / 2f);

            // since we're not restoring from a saved attack rate value, we are safe
            // to increase our attack bonus here.
            Player.Instance.AttackRate = 1 + dex_attack_speed_bonus;

        }

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Load Player Items %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // add item ids
        if (save_game_object.ItemIds.Count > 0)
        { 
            
            // iterate through the item ids, add them only if we don't already have them
            foreach(var item_id in save_game_object.ItemIds)
            { 

                // if the player doesn't have the item, add it
                if(!Player.Instance.HasItem(item_id))
                { 
                    Player.Instance.AddItem(item_id);
                }

            }

        }

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Load Player Weapons %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // add item ids
        if (save_game_object.WeaponIds.Count > 0)
        {

            // iterate through the item ids, add them only if we don't already have them
            foreach(var weapon_id in save_game_object.WeaponIds)
            {

                // if the player doesn't have a weapon and it shows we're supposed to have it, add it
                if(!Player.Instance.Weapons.Contains(weapon_id))
                {

                    // if the weapon_id is empty, just continue the loop
                    if (weapon_id == WeaponType.None)
                        continue;

                    // grant the weapon here
                    Player.Instance.GrantWeapon(weapon_id);

                }
                
            }

        }

        // reset crit and rage bonuses on load
        save_game_object.LuckyCritOvercap = 0.0f;
        save_game_object.RageBonus = 0.0f;

        // set the current save game as the loaded save game 
        this.current_save_game = save_game_object;

        // return indicating success
        return true;

    }

    // attempt to save mod game state
    public bool SaveModGameState
    (
        String save_file, 
        AdventureModSaveGame save_game_info
    )
    {
        
        // ensure our player instance isn't null
        if(Player.Instance == null)
        {
            this.ModLogger("ERROR", "Tried to call SaveModGameState while player instance is null.");
            return false;
        }
        
        // ensure we have save game info
        if (save_game_info == null)
        {
            this.ModLogger("WARNING", "Unable to save save game.  No savegame info to save.");
            return false;
        }

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Check for First Saves %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // if no file exists, we have to do some prep work
        if(!File.Exists(save_file))
        {

            // set notice
            this.ModLogger("NOTICE", "Save file does not exist when saving.  Likely a first save.");
            
            // set player default bombs
            Player.Instance.Bombs = 5;

            // set player keys
            Player.Instance.Keys = 3;
            
            // set player coins
            Player.Instance.Coins = 0;

            // current playthrough
            this.Playthrough = 1;
            
            // set max-health to 10
            Player.Instance.Damageable.MaxHealth = 10;

            // heal the player to max health
            Player.Instance.Damageable.Heal(10f);

        }
        else
        {
            
            // check max health value (prevents saving in bad states that may appear
            // when backend level number changes occur)
            if (Player.Instance.Damageable.MaxHealth < 10)
            {
                this.ModLogger("ERROR", "Tried to call SaveModGameState on a player instance that had mis-matched max health values (aka trying to save on a bad level).  Returning without saving.");
                return false;
            }

        }

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Set Basic Player Information %%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        // get max-health
        save_game_info.Maxhealth = Player.Instance.Damageable.MaxHealth;

        // get current health
        save_game_info.Health = Player.Instance.Damageable.Health;
        
        // get coins
        save_game_info.Coins = Player.Instance.Coins;

        // get bombs
        save_game_info.Bombs = Player.Instance.Bombs;

        // set keys
        save_game_info.Keys = Player.Instance.Keys;


        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Track Level Number %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // set level number as current
        save_game_info.LevelNumber = Level.Instance.LevelNumber;

        // switch on the level number and reset level to level 1 if necessary
        switch(save_game_info.LevelNumber)
        {
            case 6:
            case 7:
                save_game_info.LevelNumber = 1;
                break;
            default:
                break;
        }
    
        

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Generate Items Array %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        // item ids currently posessed
        List<ItemId> ItemIds;

        // if the player has items, save them
        if (Player.Instance.Items.Count > 0)
        {

            // rese item ids before storing items
            save_game_info.ItemIds = new List<ItemId>();

            // walk items and set add ids
            foreach (var item in Player.Instance.Items)
            {

                    // set savegame item ids
                    save_game_info.ItemIds.Add(item.TypeId);
                
            }

        }


        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Generate Weapons Array %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // weapon ids currently posessed
        List<WeaponType> WeaponIds;

        // if the player has items, save them
        if (Player.Instance.Weapons.Count > 0)
        {

            // rese item ids before storing items
            save_game_info.WeaponIds = new List<WeaponType>();

            // add weapons
            foreach (var weapon in Player.Instance.Weapons)
            {

                // set weapon id
                save_game_info.WeaponIds.Add(weapon);

            }
                        
        }


        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Generate Weapons Array %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // create json from json info
        var save_as_json = JsonUtility.ToJson(save_game_info);
        if (save_as_json == null)
            return false;
        
        // write our json to disk
        System.IO.File.WriteAllText(save_file, save_as_json);
        
        // return indicating success
        return true;

    }


    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%% Melee Mode %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // init melee mode to false
    public bool melee_mode_enabled = false;
    

    // Call this to enable melee mode.  In melee mode a health regen timer is activated to give your
    // player health regeneration.  It should make melee builds viable.
    public bool EnableMeleeMode()
    {

        this.ModLogger("NOTICE", "Attempting to enable melee mode");

        // ensure melee mode hasn't already been loaded
        if (this.melee_mode_enabled == true)
            return true;

        this.ModLogger("NOTICE", "Melee mode enabled!");

        // set melee mode enabled flag to true
        this.melee_mode_enabled = true;
        
        // give the player a starting melee weapon to go along with their ranged weapon (for melee mode)
        Player.Instance.GrantWeapon(WeaponType.FirmitasGlaive);
        

        // return indicating success
        return true;

    }
    

    // check if a player is dual weilding or not
    public bool PlayerIsUsingTwoMeleeWeapons()
    {

        // count of how many hands are currently weilding melee weapons
        int hands_melee = 0;

        // Check to see if a player has ranged weapons equipped, if they do
        // do not give health regen bonus.
        foreach (var hand in Player.Instance.Hands)
        {

            // if our hand has a weapon check to ensure it's not ranged
            if (hand.CurrentWeapon != null)
            {

                // return if it's a ranged type (staff/bow), if not just break and do regen
                switch (hand.CurrentWeapon.Type)
                {
                    case WeaponType.ComitasStaff:
                    case WeaponType.DisiplinaBow:
                    case WeaponType.JustitiaStaff:
                        break;
                    default:
                        hands_melee++;
                        break;
                }

            }

        }

        // if we are dual weidling weapons, return true
        if (hands_melee == 2)
            return true;

        // return false if we are not dual weilding
        return false;
        
    }
    
}

