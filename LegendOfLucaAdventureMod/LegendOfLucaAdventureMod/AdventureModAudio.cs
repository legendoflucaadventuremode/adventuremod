﻿using ClipperLib;
using System;
using Legend;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using UnityEngine;
using X_UniTMX;
using System.Diagnostics;

namespace LegendOfLucaAdventureMod
{

    public class AdventureModAudio : MonoBehaviour
    {

        //compatible file extensions
        string[] valid_file_types = { "wav" };

        // set audio clips
        public Dictionary<string, AudioClip> clips;

        // attempt to load mod custom sounds
        public bool LoadModDefaultSounds(LegendOfLucaAdventureMode mod_cb)
        {

            // mod has to be loaded and instance has to be set before sounds can be loaded
            if (LegendOfLucaAdventureMode.Instance == null)
                return false;
            
            // only create clips if we don't already have any
            if (this.clips != null)
                return false;

            // add logger message
            mod_cb.ModLogger("NOTICE", "Attempting to load default mod clips!");

            // create new clips dictionary
            this.clips = new Dictionary<string, AudioClip>();

            // generate path to directory
            string path_to_dir = Path.GetFullPath(Application.dataPath + "\\..\\" + mod_cb.WavFilesystemFolder);
            
            // attempt to get all files
            string[] files = Directory.GetFiles(path_to_dir);

            foreach(var file in files)
            {
                // mod_cb.ModLogger("NOTICEFILEHERE", Path.GetFileName(file));
                //StartCoroutine(LoadSoundFromFilePath("NFF-bacteria.wav", "NFF-bacteria.wav"));

                // get filename only (since relative path is built)
                string filename = Path.GetFileName(file);

                // only load valid types
                if (!this.IsValidType(filename))
                    continue;

                // start path loader
                StartCoroutine(LoadSoundFromRelativeFilePath(filename, filename));

            }

            // return indicating success
            return true;

        }

        

        // checks to see if a file is a valid type
        public bool IsValidType(string file_load_path)
        {

            // run null check
            if (file_load_path == null)
                return false;

            // check for valid types
            foreach (string ext in valid_file_types)
            {

                // return indicating good type
                if (file_load_path.IndexOf(ext) > -1) return true;

            }

            // return indicating bad type
            return false;
          
        }

        // set load iter
        public int load_iter = 0;

        // add a sound to the clips (unique keys only)
        IEnumerator LoadSoundFromRelativeFilePath(string key, string relative_file_load_path)
        {

            // ensure we have a callback
            LegendOfLucaAdventureMode mod_cb = LegendOfLucaAdventureMode.Instance;

            // generate path to file
            string path_to_file = Path.GetFullPath(Path.Combine(Application.dataPath + "\\..\\" + mod_cb.WavFilesystemFolder, relative_file_load_path));

            // replace slashes
            path_to_file = path_to_file.Replace('\\', '/');
            
            // log creation attempt
            mod_cb.ModLogger("NOTICE", String.Format("Attempting to load {0}.", path_to_file));

            // set clip loader
            WWW clip_loader = new WWW("file://" + path_to_file);
            
            // wait for the clip to become ready to play
            while (!clip_loader.audioClip.isReadyToPlay)
            {

                // increment the load iterator
                this.load_iter++;
                
                // if our load iter is over 100, break
                if (load_iter > 100)
                {
                    this.load_iter = 0;
                    yield break;
                }
                else
                {
                    // return the clip loader
                    yield return clip_loader;
                }

            }
            
            // add clip if not null
            if(clip_loader.GetAudioClip(false) != null)
            { 
                // add clip
                this.clips.Add(key, clip_loader.GetAudioClip(false));
                        
                // log notice
                mod_cb.ModLogger("NOTICE", String.Format("Loaded {0} ok!", path_to_file));
            }

        }

    }

};

