﻿using ClipperLib;
using System;
using Legend;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using UnityEngine;
using X_UniTMX;
using System.Diagnostics;
using LegendOfLucaAdventureMod;


namespace LegendOfLucaAdventureMod
{
    public class AdventureModRoomTracker
    {

        // room enter callback
        public void OnRoomEnter(Room room, Player player_cb,  Level level_cb, LegendOfLucaAdventureMode mod_cb )
        {

            if (level_cb.LevelNumber == 0)
                return;
            if (level_cb.LevelNumber == 6)
                return;
            if (level_cb.LevelNumber == 7)
                return;

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Apply Explorer Bonus %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // increment the room count if it hasn't been cleared yet
            if (!room.IsCleared)
                mod_cb.current_save_game.RoomEnterCount++;

            // add room enter bonus
            if(mod_cb.current_save_game.RoomEnterCount % 500 == 0)
            {

                // display explorer bonus
                HUD.ShowMessage("Explorer Bonus for Entering 500 rooms!", "+50 all stats!");

                // add 50 to all stats
                mod_cb.current_save_game.Dexterity += 50;
                mod_cb.current_save_game.Strength += 50;
                mod_cb.current_save_game.Intelligence += 50;
                mod_cb.current_save_game.Evasion += 50;

            }

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Perform Tithing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            // perform tithing
            if (Player.Instance.Coins >= 255)
            {

                // create a random tithe between 200 and 255
                System.Random tithe_random = new System.Random();
                int tithe_amount = tithe_random.Next(200, 255);

                // display tithe message
                HUD.ShowMessage("Tithing.", "You give of yourself to selfish gods, but only for something in return, as is the nature of all who tithe.  255 coins removed, 10 Permanent Str/Dex/Int added.", 8f);

                // add save game str/dex/int bonuses
                LegendOfLucaAdventureMode.Instance.current_save_game.Strength += 10;
                LegendOfLucaAdventureMode.Instance.current_save_game.Dexterity += 10;
                LegendOfLucaAdventureMode.Instance.current_save_game.Intelligence += 10;

                // reduce player coins
                Player.Instance.Coins -= tithe_amount;

                // reduce coins in savegame as well
                LegendOfLucaAdventureMode.Instance.current_save_game.Coins = Player.Instance.Coins;
                
            }

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Perform Explosive Maintainence %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // perform tithing
            if (Player.Instance.Bombs >= 65)
            {

                // create a random tithe between 200 and 255
                System.Random explosive_maintainence_random = new System.Random();
                int reduction_amount = explosive_maintainence_random.Next(20, 35);

                // display tithe message
                HUD.ShowMessage("Explosive Maintainence.", "The weight of many explosives has strengthened your body.  The burden of carrying them has strengthend your mind.  The weight of their removal has made you more nimble. (bombs were removed, but str/dex/int goes up by 20)", 8f);

                // add save game str/dex/int bonuses
                LegendOfLucaAdventureMode.Instance.current_save_game.Strength += 20;
                LegendOfLucaAdventureMode.Instance.current_save_game.Dexterity += 20;
                LegendOfLucaAdventureMode.Instance.current_save_game.Intelligence += 20;

                // reduce player bombs
                Player.Instance.Bombs -= reduction_amount;

                // reduce coins in savegame as well
                LegendOfLucaAdventureMode.Instance.current_save_game.Bombs = Player.Instance.Bombs;

            }
            
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Perform Key To Everything %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // perform tithing
            if (Player.Instance.Keys >= 50)
            {

                // create a random 
                System.Random key_random = new System.Random();
                int reduction_amount = key_random.Next(15, 40);

                // display tithe message
                HUD.ShowMessage("Missing Keys.", "The key to a long life is a healthy outlook, which you can find outside, with the cat.  (some keys removed but full health restored)", 8f);
                                
                // reduce player bombs
                Player.Instance.Keys -= reduction_amount;

                // reduce coins in savegame as well
                LegendOfLucaAdventureMode.Instance.current_save_game.Keys = Player.Instance.Keys;

            }

            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%% Special Rooms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            // if the room isn't cleared yet, check it to see what kind of room it could be
            if (!room.IsCleared)
            { 

                // create new randomizer to determine potential room types
                System.Random room_rng = new System.Random();


                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%% Damp Room %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                
                // damp rooms destroy all player ammo
                int percentage_chance_of_damp_room = 5;

                // calculate if this is a damp room
                int is_damp_room = room_rng.Next(1, 100);
                if (is_damp_room <= percentage_chance_of_damp_room)
                {
                    this.StartDampRoomEvent(room, player_cb, level_cb, mod_cb);
                }


                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%% Lucky Rooms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                // lucky rooms increase the characters crit chance
                // by 20% for 5 minutes
                int percentage_chance_of_lucky_room = 4 + mod_cb.current_save_game.Playthrough;

                // calculate if this is a lucky room
                int is_lucky_room = room_rng.Next(1, 100);
                if(is_lucky_room <= percentage_chance_of_lucky_room)
                {
                    this.StartLuckyRoomEvent(room, player_cb, level_cb, mod_cb);
                }

                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%% Poison Rooms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                // Poison rooms start a slow health degen (1/2heart second for 5 seconds).  Does not kill, only reduces.
                int percentage_chance_of_poison_room = 5;
                                
                // calculate if this is a poison room
                int is_poison_room = room_rng.Next(1, 100);
                if(is_poison_room <= percentage_chance_of_poison_room)
                {
                    this.StartPoisonRoomEvent(room, player_cb, level_cb, mod_cb);
                }

                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%% Rage Rooms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                // Chance of rage room.  Damage temporarily increased by 25% for 5 minutes.
                int percentage_chance_of_rage_room = 4 + mod_cb.current_save_game.Playthrough;

                // calculate if this is a rage room
                int is_rage_room = room_rng.Next(1, 100);
                if (is_rage_room <= percentage_chance_of_rage_room)
                {
                    this.StartRageRoomEvent(room, player_cb, level_cb, mod_cb);
                }

                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%% Blood Rooms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                // Chance of encountering a blood room.  Blood rooms slowly regenerate player
                // life due to residual necromantic magic in the air.
                int percentage_chance_of_blood_room = 4 + mod_cb.current_save_game.Playthrough;

                // calculate if this is a blood room
                int is_blood_room = room_rng.Next(1, 100);
                if (is_blood_room <= percentage_chance_of_blood_room)
                {
                    this.StartBloodRoomEvent(room, player_cb, level_cb, mod_cb);
                }

                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%% Greed Rooms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                // Chance of encountering a greed room.  Greed rooms create a bonus that grants
                // players 1 coin a second coin bonus for 30 seconds.
                int percentage_chance_of_greed_room = 4 + mod_cb.current_save_game.Playthrough;

                // calculate if this is a greed room
                int is_greed_room = room_rng.Next(1, 100);
                if (is_greed_room <= percentage_chance_of_greed_room)
                {
                    this.StartGreedRoomEvent(room, player_cb, level_cb, mod_cb);
                }

                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%% Armory Room %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                // Chance of encountering an armory room.  Armory rooms grant the player a bonus
                // of regenerating one bomb per second for 20 seconds.
                int percentage_chance_of_armory_room = 4 + mod_cb.current_save_game.Playthrough;

                // calculate if this is a armory room
                int is_armory_room = room_rng.Next(1, 100);
                if (is_armory_room <= percentage_chance_of_armory_room)
                {
                    this.StartArmoryRoomEvent(room, player_cb, level_cb, mod_cb);
                }

            }
            
            // save the game state when you enter a new room
            mod_cb.SaveModGameState(mod_cb.DefaultSaveFile, mod_cb.current_save_game);

            // load mod state from save (increases dex attack rate among other things)
            mod_cb.LoadModGameState(mod_cb.DefaultSaveFile);

        }

        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Room Type Events %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        // If any of these is set to true, their respective room modes
        // will become active in the global timer.  Room modes are subsequently
        // deactivated in the timer when they are finished.
        public bool damp_room_event_active = false;
        public bool lucky_room_event_active = false;
        public bool poison_room_event_active = false;
        public bool rage_room_event_active = false;
        public bool blood_room_event_active = false;
        public bool greed_room_event_active = false;
        public bool armory_room_event_active = false;

        // these are used to monitor how many seconds each room bonus is active for
        public int damp_room_event_seconds_elapsed = 0;
        public int lucky_room_event_seconds_elapsed = 0;
        public int poison_room_event_seconds_elapsed = 0;
        public int rage_room_event_seconds_elapsed = 0;
        public int blood_room_event_seconds_elapsed = 0;
        public int greed_room_event_seconds_elapsed = 0;
        public int armory_room_event_seconds_elapsed = 0;

        // start damp room event
        public void StartDampRoomEvent(Room room, Player player_cb, Level level_cb, LegendOfLucaAdventureMode mod_cb)
        {

            // lucky rooms are also green (to throw players off)
            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.blue;

            // start event
            this.damp_room_event_active = true;
            this.damp_room_event_seconds_elapsed = 0;
            HUD.ShowMessage("Damp Room", "The gunpowder in your bombs has started to go bad.", 5f);

            // set room sound
            string room_sound = "rain_and_light_thunder.wav";

            // attempt to play sound
            if (mod_cb.mod_audio.clips.ContainsKey(room_sound))
                Player.Instance.PlaySound(mod_cb.mod_audio.clips[room_sound], 0.35f);
            else
                mod_cb.ModLogger("ERROR", "Failed to play damp room sound.");


        }

        // start lucky room event
        public void StartLuckyRoomEvent(Room room, Player player_cb, Level level_cb, LegendOfLucaAdventureMode mod_cb)
        {
            
            // lucky rooms are also green (to throw players off)
            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.green;

            // start event
            this.lucky_room_event_active = true;
            this.lucky_room_event_seconds_elapsed = 0;
            HUD.ShowMessage("Lucky Room", "You begin to feel as if every cut you make will strike a vein.", 5f);

            // set room sound
            string room_sound = "NFF-wind-gust-2.wav";
            
            // attempt to play sound
            if (mod_cb.mod_audio.clips.ContainsKey(room_sound))
                Player.Instance.PlaySound(mod_cb.mod_audio.clips[room_sound], 0.5f);
            else
                mod_cb.ModLogger("ERROR", "Failed to play lucky room sound.");
              

        }

        // start poison room event
        public void StartPoisonRoomEvent(Room room, Player player_cb,  Level level_cb, LegendOfLucaAdventureMode mod_cb)
        {

            // change lightning
            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.green;
            this.poison_room_event_active = true;
            this.poison_room_event_seconds_elapsed = 0;
            HUD.ShowMessage("Poison Room", "You can't breath, you gasp for breath as your life begins to drain from you.", 5f);

            // set room sound
            string room_sound = "bubbling2.wav";
            
             // attempt to play sound
             if (mod_cb.mod_audio.clips.ContainsKey(room_sound))
                Player.Instance.PlaySound(mod_cb.mod_audio.clips[room_sound], 0.5f);
             else
                 mod_cb.ModLogger("ERROR", "Failed to play poison room sound.");
                

         }

         // start rage room event
         public void StartRageRoomEvent(Room room, Player player_cb, Level level_cb, LegendOfLucaAdventureMode mod_cb)
         {

             // change lighting in room
             foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.magenta;
             this.rage_room_event_active = true;
             this.rage_room_event_seconds_elapsed = 0;
             HUD.ShowMessage("Rage Room", "Your heart fills with hatred, your mind fills with madness.  Your strength is increased.", 5f);

             // set room sound
             string room_sound = "angry_man.wav";
             
             // attempt to play sound
             if (mod_cb.mod_audio.clips.ContainsKey(room_sound))
                Player.Instance.PlaySound(mod_cb.mod_audio.clips[room_sound], 0.5f);
             else
                 mod_cb.ModLogger("ERROR", "Failed to play rage room sound.");
             

        }

        // start blood room event
        public void StartBloodRoomEvent(Room room, Player player_cb, Level level_cb, LegendOfLucaAdventureMode mod_cb)
        {

            // change lighting in room
            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.red;
            this.blood_room_event_active = true;
            this.blood_room_event_seconds_elapsed = 0;
            HUD.ShowMessage("Blood Room Discovered", "This room is filled with remaining necromantic energy, filling you slowly with life.", 5f);
            
            // set room sound
            string room_sound = "NFF-black-energy-02.wav";
                        
            
            // attempt to play sound
            if (mod_cb.mod_audio.clips.ContainsKey(room_sound))
                Player.Instance.PlaySound(mod_cb.mod_audio.clips[room_sound], 0.5f);
            else
                mod_cb.ModLogger("ERROR", "Failed to play blood room sound.");
                

        }

        // start greed room event
        public void StartGreedRoomEvent(Room room, Player player_cb, Level level_cb, LegendOfLucaAdventureMode mod_cb)
        {

            // change lighting in room
            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.yellow;
            this.greed_room_event_active = true;
            this.greed_room_event_seconds_elapsed = 0;
            HUD.ShowMessage("Greed Room Discovered", "Mind over matter.  Matter into pocket.", 5f);
            
            // set room sound
            string room_sound = "cash_register2.wav";
            
            // attempt to play sound
            if (mod_cb.mod_audio.clips.ContainsKey(room_sound))
                Player.Instance.PlaySound(mod_cb.mod_audio.clips[room_sound], 0.5f);
            else
                mod_cb.ModLogger("ERROR", "Failed to play greed room sound.");
                
        }

        // start armory room event
        public void StartArmoryRoomEvent(Room room, Player player_cb, Level level_cb, LegendOfLucaAdventureMode mod_cb)
        {
            
            // change lighting in room
            foreach (var light in GameObject.FindObjectsOfType<Light>()) light.color = Color.cyan;

            this.armory_room_event_active = true;
            this.armory_room_event_seconds_elapsed = 0;
            HUD.ShowMessage("Armory Room Discovered", "Heavenly mayhem granted one at a time.", 5f);
            
            // set room sound
            string room_sound = "war_x.wav";

            
            // attempt to play sound
            if (mod_cb.mod_audio.clips.ContainsKey(room_sound))
                Player.Instance.PlaySound(mod_cb.mod_audio.clips[room_sound], 0.5f);
            else
                mod_cb.ModLogger("ERROR", "Failed to play armory room sound.");
           
        }



        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%% Global Room Timer Loop %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

       
        

    }

}
